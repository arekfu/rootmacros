double massFromTP(const double t, const double p) {

	const double mass = -(t*t-p*p)/(2.*t);
	return mass;

}
