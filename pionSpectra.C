void pionSpectra(char *fname1, char *fname2, float factor, char *fout) {
	gStyle->SetOptLogy();
	gStyle->SetOptStat(0);
	gStyle->SetFillColor(kWhite);
	gStyle->SetDrawBorder(0);

	TFile f1(fname1);
	TTree *t1;
	bool isOld1 = true;
	t1 = (TTree *) gROOT->FindObject("h101");
	if(!t1) {
		t1 = (TTree *) gROOT->FindObject("et");
		isOld1 = false;
	}
	TFile f2(fname2);
	TTree *t2;
	bool isOld2 = true;
	t2 = (TTree *) gROOT->FindObject("h101");
	if(!t2) {
		t2 = (TTree *) gROOT->FindObject("et");
		isOld2 = false;
	}
	t2->SetLineColor(kRed);

	stringstream cutpip1, cutpi01, cutpim1, cutpip2, cutpi02, cutpim2;
	if(isOld1) {
		cutpip1 << "(Avv==-1 && Zvv==1)";
		cutpi01 << "(Avv==-1 && Zvv==0)";
		cutpim1 << "(Avv==-1 && Zvv==-1)";
	} else {
		cutpip1 << "(A==0 && Z==1)";
		cutpi01 << "(A==0 && Z==0)";
		cutpim1 << "(A==0 && Z==-1)";
	}

	if(isOld2) {
		cutpip2 << factor << "*(Avv==-1 && Zvv==1)";
		cutpi02 << factor << "*(Avv==-1 && Zvv==0)";
		cutpim2 << factor << "*(Avv==-1 && Zvv==-1)";
	} else {
		cutpip2 << factor << "*(A==0 && Z==1)";
		cutpi02 << factor << "*(A==0 && Z==0)";
		cutpim2 << factor << "*(A==0 && Z==-1)";
	}

	TCanvas c1;
	c1.Divide(3);
	c1_1->cd();
	if(isOld1) {
		t1->SetLineStyle(kSolid);
		t1->Draw("Enerj", cutpip1.str().c_str());
	} else {
		t1->SetLineStyle(kSolid);
		t1->Draw("EKin", cutpip1.str().c_str());
	}
	if(isOld2) {
		t2->SetLineStyle(kSolid);
		t2->Draw("Enerj", cutpip2.str().c_str(), "same");
	} else {
		t2->SetLineStyle(kSolid);
		t2->Draw("EKin", cutpip2.str().c_str(), "same");
	}
	c1_1->BuildLegend();
	c1_2->cd();
	if(isOld1) {
		t1->SetLineStyle(kSolid);
		t1->Draw("Enerj", cutpi01.str().c_str());
	} else {
		t1->SetLineStyle(kSolid);
		t1->Draw("EKin", cutpi01.str().c_str());
	}
	if(isOld2) {
		t2->SetLineStyle(kSolid);
		t2->Draw("Enerj", cutpi02.str().c_str(), "same");
	} else {
		t2->SetLineStyle(kSolid);
		t2->Draw("EKin", cutpi02.str().c_str(), "same");
	}
	c1_2->BuildLegend();
	c1_3->cd();
	if(isOld1) {
		t1->SetLineStyle(kSolid);
		t1->Draw("Enerj", cutpim1.str().c_str());
	} else {
		t1->SetLineStyle(kSolid);
		t1->Draw("EKin", cutpim1.str().c_str());
	}
	if(isOld2) {
		t2->SetLineStyle(kSolid);
		t2->Draw("Enerj", cutpim2.str().c_str(), "same");
	} else {
		t2->SetLineStyle(kSolid);
		t2->Draw("EKin", cutpim2.str().c_str(), "same");
	}
	c1_3->BuildLegend();
	c1.Print(fout);
}
