#ifndef PARTICLETYPE_H_
#define PARTICLETYPE_H_

#include "TString.h"
#include <sstream>

enum ParticleType {
  Neutron,
  Proton,
  PiPlus,
  PiZero,
  PiMinus,
  Deuteron,
  Triton,
  Helium3,
  Alpha,
  Unknown
};

TString elementName(const int Z);

TString AZToNucleusLabel(const int A, const int Z) {
  TString ss;
  if(A>0) {
    ss += "^{";
    ss += A;
    ss += "}";
  }
  return ss + elementName(Z);
}

TString AZToLabel(const int A, const int Z) {
  switch(A) {
    case 0:
      switch(Z) {
        case 1:
          return "#pi^{+}";
          break;
        case 0:
          return "#pi^{0}";
          break;
        case -1:
          return "#pi^{-}";
          break;
        default:
          return "";
      }
      break;
    case 1:
      switch(Z) {
        case 0:
          return "n";
          break;
        case 1:
          return "p";
          break;
        default:
          return "";
      }
      break;
    case 2:
      switch(Z) {
        case 1:
          return "d";
          break;
        default:
          return "";
      }
      break;
    case 3:
      switch(Z) {
        case 1:
          return "t";
          break;
        case 2:
          return "^{3}He";
          break;
        default:
          return "";
      }
      break;
    case 4:
      switch(Z) {
        case 2:
          return "#alpha";
          break;
        default:
          return "";
      }
      break;
    default:
      TString ss;
      if(A>0) {
        ss += "^{";
        ss += A;
        ss += "}";
      }
      return ss + elementName(Z);
      break;
  }
}

TString AZToSuffix(const int A, const int Z) {
  switch(A) {
    case 0:
      switch(Z) {
        case 1:
          return "pi+";
          break;
        case 0:
          return "pi0";
          break;
        case -1:
          return "pi-";
          break;
        default:
          return "";
      }
      break;
    case 1:
      switch(Z) {
        case 0:
          return "n";
          break;
        case 1:
          return "p";
          break;
        default:
          return "";
      }
      break;
    case 2:
      switch(Z) {
        case 1:
          return "d";
          break;
        default:
          return "";
      }
      break;
    case 3:
      switch(Z) {
        case 1:
          return "t";
          break;
        case 2:
          return "He3";
          break;
        default:
          return "";
      }
      break;
    case 4:
      switch(Z) {
        case 2:
          return "a";
          break;
        default:
          return "";
      }
      break;
    default:
      std::stringstream ss;
      if(A>0)
        ss << A;
      return elementName(Z) + TString(ss.str());
      break;
  }
}

TString AZToSuffixEscaped(const int A, const int Z) {
  switch(A) {
    case 0:
      switch(Z) {
        case 1:
          return "pi\\+";
          break;
        case 0:
          return "pi0";
          break;
        case -1:
          return "pi-";
          break;
        default:
          return "";
      }
      break;
    case 1:
      switch(Z) {
        case 0:
          return "n";
          break;
        case 1:
          return "p";
          break;
        default:
          return "";
      }
      break;
    case 2:
      switch(Z) {
        case 1:
          return "d";
          break;
        default:
          return "";
      }
      break;
    case 3:
      switch(Z) {
        case 1:
          return "t";
          break;
        case 2:
          return "He3";
          break;
        default:
          return "";
      }
      break;
    case 4:
      switch(Z) {
        case 2:
          return "a";
          break;
        default:
          return "";
      }
      break;
    default:
      std::stringstream ss;
      if(A>0)
        ss << A;
      return elementName(Z) + TString(ss.str());
      break;
  }
}

TString AZToSuffix(const int A0, const int Z0, const int ATol, const int ZTol, const bool includeElement=true) {
  TString suffix = "(";
  for(int Z=Z0-ZTol; Z<=Z0+ZTol; ++Z) {
    for(int A=A0-ATol; A<=A0+ATol; ++A) {
      suffix += AZToSuffix(A,Z) + "|";
    }
    if(includeElement)
      suffix += elementName(Z) + "|";
  }
  suffix[suffix.Length()-1]=')';
  return suffix;
}

TString AZToSuffixEscaped(const int A0, const int Z0, const int ATol, const int ZTol, const bool includeElement=true) {
  TString suffix = "(";
  for(int Z=Z0-ZTol; Z<=Z0+ZTol; ++Z) {
    for(int A=A0-ATol; A<=A0+ATol; ++A) {
      suffix += AZToSuffixEscaped(A,Z) + "|";
    }
    if(includeElement)
      suffix += elementName(Z) + "|";
  }
  suffix[suffix.Length()-1]=')';
  return suffix;
}

TString particleTypeToSuffix(ParticleType const t) {
  switch(t) {
    case Neutron:
      return "n";
      break;
    case Proton:
      return "p";
      break;
    case PiPlus:
      return "pi+";
      break;
    case PiZero:
      return "pi0";
      break;
    case PiMinus:
      return "pi-";
      break;
    case Deuteron:
      return "d";
      break;
    case Triton:
      return "t";
      break;
    case Helium3:
      return "He3";
      break;
    case Alpha:
      return "a";
      break;
    default:
      return "";
      break;
  }
}

TString particleTypeToDirectory(ParticleType const t) {
  switch(t) {
    case Neutron:
      return "neutron";
      break;
    case Proton:
    case PiPlus:
    case PiMinus:
    case Deuteron:
    case Triton:
    case Helium3:
    case Alpha:
      return "lcp";
      break;
    case PiZero:
    default:
      return "unavailable";
      break;
  }
}

Int_t particleTypeToA(ParticleType const t) {
  switch(t) {
    case PiMinus:
    case PiZero:
    case PiPlus:
      return 0;
      break;
    case Neutron:
    case Proton:
      return 1;
      break;
    case Deuteron:
      return 2;
      break;
    case Triton:
    case Helium3:
      return 3;
      break;
    case Alpha:
      return 4;
      break;
    default:
      return 9999;
      break;
  }
}

Int_t particleTypeToZ(ParticleType const t) {
  switch(t) {
    case PiMinus:
      return -1;
      break;
    case Neutron:
    case PiZero:
      return 0;
      break;
    case Proton:
    case PiPlus:
    case Deuteron:
    case Triton:
      return 1;
      break;
    case Helium3:
    case Alpha:
      return 2;
      break;
    default:
      return 9999;
      break;
  }
}

ParticleType AZToParticleType(const Int_t A, const Int_t Z) {
  switch(A) {
    case 0: // pion
      switch(Z) {
        case 1:
          return PiPlus;
          break;
        case 0:
          return PiZero;
          break;
        case -1:
          return PiMinus;
          break;
        default:
          return Unknown;
          break;
      }
      break;
    case 1: // nucleon
      switch(Z) {
        case 1:
          return Proton;
          break;
        case 0:
          return Neutron;
          break;
        default:
          return Unknown;
          break;
      }
      break;
    case 2: // d
      switch(Z) {
        case 1:
          return Deuteron;
          break;
        default:
          return Unknown;
          break;
      }
      break;
    case 3: // d
      switch(Z) {
        case 1:
          return Triton;
          break;
        case 2:
          return Helium3;
          break;
        default:
          return Unknown;
          break;
      }
      break;
    case 4: // d
      switch(Z) {
        case 2:
          return Alpha;
          break;
        default:
          return Unknown;
          break;
      }
      break;
    default:
      return Unknown;
      break;
  }
}

// Table of chemical element names
const static int elementTableSize = 113; // up to Cn
const TString elementTable[elementTableSize] = {
  "",
  "H",
  "He",
  "Li",
  "Be",
  "B",
  "C",
  "N",
  "O",
  "F",
  "Ne",
  "Na",
  "Mg",
  "Al",
  "Si",
  "P",
  "S",
  "Cl",
  "Ar",
  "K",
  "Ca",
  "Sc",
  "Ti",
  "V",
  "Cr",
  "Mn",
  "Fe",
  "Co",
  "Ni",
  "Cu",
  "Zn",
  "Ga",
  "Ge",
  "As",
  "Se",
  "Br",
  "Kr",
  "Rb",
  "Sr",
  "Y",
  "Zr",
  "Nb",
  "Mo",
  "Tc",
  "Ru",
  "Rh",
  "Pd",
  "Ag",
  "Cd",
  "In",
  "Sn",
  "Sb",
  "Te",
  "I",
  "Xe",
  "Cs",
  "Ba",
  "La",
  "Ce",
  "Pr",
  "Nd",
  "Pm",
  "Sm",
  "Eu",
  "Gd",
  "Tb",
  "Dy",
  "Ho",
  "Er",
  "Tm",
  "Yb",
  "Lu",
  "Hf",
  "Ta",
  "W",
  "Re",
  "Os",
  "Ir",
  "Pt",
  "Au",
  "Hg",
  "Tl",
  "Pb",
  "Bi",
  "Po",
  "At",
  "Rn",
  "Fr",
  "Ra",
  "Ac",
  "Th",
  "Pa",
  "U",
  "Np",
  "Pu",
  "Am",
  "Cm",
  "Bk",
  "Cf",
  "Es",
  "Fm",
  "Md",
  "No",
  "Lr",
  "Rf",
  "Db",
  "Sg",
  "Bh",
  "Hs",
  "Mt",
  "Ds",
  "Rg",
  "Cn"
};

TString elementName(const int Z) {
  if(Z>=0 && Z<elementTableSize)
    return elementTable[Z];
  else
    return "";
}

#endif // PARTICLETYPE_H_
