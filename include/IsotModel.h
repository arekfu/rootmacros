#ifndef ISOTMODEL_H
#define ISOTMODEL_H
#include "TString.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2F.h"
#include "DatasetLibrary.h"
#include "ModelLook.h"
#include <vector>
#include <utility>

class IsotHisto2D {
  public:
    IsotHisto2D() :
      histo(NULL)
  {}

    IsotHisto2D(TH2F * const h, const char * const t) :
      histo(h),
      title(t)
  {}

    TH2F *getHisto2D() const {
      return histo;
    }

    ~IsotHisto2D() {
    }

    TH2F *histo;
    TString title;

};

typedef std::vector<IsotHisto2D> IsotHisto2DVector;
typedef std::vector<IsotHisto2D>::iterator IsotHisto2DIter;

class IsotHisto {
  public:
    IsotHisto() :
      histo(NULL)
  {}

    IsotHisto(TH1D * const h, const char * const t) :
      histo(h),
      title(t)
  {}

    TH1D *getHisto() const {
      return histo;
    }

    ~IsotHisto() {
    }

    TH1D *histo;
    TString title;

};

typedef std::vector<IsotHisto> IsotHistoVector;
typedef std::vector<IsotHisto>::iterator IsotHistoIter;

class IsotHistos {
  public:
    IsotHistos() :
      histos(NULL)
  {}

    IsotHistos(std::vector<TH1D*> * const h, const char * const t) :
      histos(h),
      title(t)
  {}

    std::vector<TH1D*> *getHistos() const {
      return histos;
    }

    ~IsotHistos() {
    }

    std::vector<TH1D*> *histos;
    TString title;

};

typedef std::vector<IsotHistos> IsotHistosVector;
typedef std::vector<IsotHistos>::iterator IsotHistosIter;

class IsotModel {
  public:
    IsotModel() :
      file(NULL)
  {}

    IsotModel(const char *f, const char *t) :
      filename(f),
      title(t),
      file(NULL)
  {}

    ~IsotModel() {
    }

    TFile *getFile() {
      if(!file)
        file = new TFile(filename);
      return file;
    }

    IsotHisto2D *getHisto(TString const &system) {
      TFile *f = getFile();
      TString histoName = "hisot_" + system(9, system.Length()-9);
      TH2F *h;
      f->GetObject(histoName.Data(), h);
      if(h)
        return new IsotHisto2D(h, title);
      else
        return NULL;
    }

    Reaction *getReaction(TString const &system) {
      Reaction *reaction;
      TFile *f = getFile();
      f->GetObject(system.Data(), reaction);
      return reaction;
    }

    TString filename;
    TString title;

  protected:
    TFile *file;

};

typedef std::vector<IsotModel> IsotModelVector;
typedef std::vector<IsotModel>::iterator IsotModelIter;

#ifdef __MAKECINT__
#pragma link C++ class IsotModel;
#pragma link C++ class IsotHisto;
#pragma link C++ class vector<IsotModel>;
#pragma link C++ class vector<IsotHisto>;
#pragma link C++ class vector<IsotModel>::iterator-;
#pragma link C++ class vector<IsotHisto>::iterator-;
#endif // __MAKECINT__

#endif
