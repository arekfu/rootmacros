#ifndef LEGENDPOSITION_H
#define LEGENDPOSITION_H

Double_t toXNDC(const double x);
Double_t toYNDC(const double y);
Int_t countPointsInside(TGraph *g, TBox *box);
void bestLegendPosition(TLegend *theLegend, std::vector<TGraph*> *graphs=NULL, std::vector<TH1*> *histos=NULL, const Double_t legendWidth=0.3, const Double_t legendHeight=0.2);

#endif // LEGENDPOSITION_H
