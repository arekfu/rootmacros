#ifndef MAXXFROMHISTOS_H_
#define MAXXFROMHISTOS_H_

double maxXFromHistos(std::list<TH1*> const &list) {
	double maxEPlot = -1.E250, maxEHisto;
	TAxis *axis;
	for(std::list<TH1*>::const_iterator i=list.begin(); i!=list.end(); ++i) {
		axis = (*i)->GetXaxis();
		maxEHisto = axis->GetBinUpEdge((*i)->FindLastBinAbove());
		if(maxEHisto>maxEPlot)
			maxEPlot = maxEHisto;
	}

	return maxEPlot;
}

#endif // MAXXFROMHISTOS_H_
