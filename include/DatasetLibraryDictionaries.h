#ifdef __MAKECINT__
#pragma link C++ class Reaction;
#pragma link C++ class DDXSReaction;
#pragma link C++ class ExcitReaction;
#pragma link C++ class IDatafile;
#pragma link C++ class IDatafile1D;
#pragma link C++ class IDatafile2D;
#pragma link C++ class DDXSDatafile;
#pragma link C++ class CCDatafile;
#pragma link C++ class MassDatafile;
#pragma link C++ class ExcitDatafile;
#pragma link C++ class IsotDatafile;
#pragma link C++ class AngDistDatafile;
#pragma link C++ class list<IDatafile1D*>;
#pragma link C++ class list<IDatafile1D*>::iterator-;
#pragma link C++ class list<DDXSDatafile*>;
#pragma link C++ class list<DDXSDatafile*>::iterator-;
#pragma link C++ class pair<Int_t, IDatafile1DList*>;
#pragma link C++ class pair<int, list<IDatafile1D*>*>;
#pragma link C++ class map<Int_t, IDatafile1DList*>;
#pragma link C++ class map<Int_t, IDatafile1DList*>::iterator-;
#pragma link C++ class map<Int_t, DDXSDatafileList*>;
#pragma link C++ class map<Int_t, DDXSDatafileList*>::iterator-;
#pragma link C++ class DDXSDataset;
#pragma link C++ class DDXSDatasetIter;
#pragma link C++ class map<int, list<IDatafile1D*>*>;
#pragma link C++ class map<int, list<IDatafile1D*>*>::iterator-;
#pragma link C++ class map<TString, IDatafile1DList*>;
#pragma link C++ class map<TString, IDatafile1DList*>::iterator-;
#pragma link C++ class vector<TGraphErrors*>;
#endif // __MAKECINT__

