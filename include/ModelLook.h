#ifndef MODELLINESTYLES_H
#define MODELLINESTYLES_H

// Model lines

struct ModelLook {
  public:
    ModelLook(const Color_t c, const Style_t s, const Width_t w) :
      color(c),
      style(s),
      width(w)
  {}
    Color_t color;
    Style_t style;
    Width_t width;
};

ModelLook getModelLook(const int i);

ModelLook getNextModelLook();

void setModelColor(const int i, const Color_t c);

void setModelStyle(const int i, const Style_t s);

void setModelWidth(const int i, const Width_t s);

void resetModelLook();

#endif

