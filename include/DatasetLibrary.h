#ifndef DATASETLIBRARY_H_
#define DATASETLIBRARY_H_

#include <map>
#include <list>
#include <fstream>
#include <iostream>
#include <utility>
#include <cmath>
#include "TString.h"
#include "TGraphErrors.h"
#include "TFile.h"
#include "TTree.h"
#include "TROOT.h"
#include "TLeaf.h"
#include "TString.h"
#include "ParticleType.h"
#include "TGraph2DErrors.h"
#include "TH2F.h"
#include "TPRegexp.h"

class Reaction : public TObject {
  public:
    Reaction() :
      Ap(0),
      Zp(0),
      Ep(0.),
      At(0),
      Zt(0)
  {}

    Reaction(TFile *f)
    {
      f->cd();
      TTree *gt = (TTree *) gROOT->FindObject("gt");

      // Read the run characteristics
      gt->GetEntry(gt->GetEntries()-1);
      At = (int) gt->GetLeaf("At")->GetValue();
      Zt = (int) gt->GetLeaf("Zt")->GetValue();
      Ap = (int) gt->GetLeaf("Ap")->GetValue();
      Zp = (int) gt->GetLeaf("Zp")->GetValue();
      Ep = (float) gt->GetLeaf("Ep")->GetValue();
    }

    Reaction(const int ap, const int zp, const float ep, const int at, const int zt) :
      Ap(ap),
      Zp(zp),
      Ep(ep),
      At(at),
      Zt(zt)
  {}

    virtual operator TString() const {
      TString s;
      if(Ap>0) {
        s += ((int)(Ep/Ap));
        s += "-AMeV ";
      } else {
        s += ((int)Ep);
        s += "-MeV ";
      }
      s += AZToLabel(Ap, Zp);
      s += " + ";
      s += AZToNucleusLabel(At, Zt);
      return s;
    }

    virtual TString toFilenameStem() const {
      TString s;
      s += AZToSuffix(Ap, Zp);
      s += '_';
      s += AZToSuffix(At, Zt);
      s += '_';
      s += ((int)Ep);
      return s;
    }

    int Ap;
    int Zp;
    float Ep;
    int At;
    int Zt;

    ClassDef(Reaction, 1);
};

class ExcitReaction : public TObject {
  public:
    ExcitReaction() :
      Ap(0),
      Zp(0),
      At(0),
      Zt(0),
      Ar(0),
      Zr(0)
  {}

    ExcitReaction(const int ap, const int zp, const int at, const int zt, const int ar, const int zr) :
      Ap(ap),
      Zp(zp),
      At(at),
      Zt(zt),
      Ar(ar),
      Zr(zr)
  {}

    virtual operator TString() const {
      TString s;
      s += AZToLabel(Ap, Zp);
      s += " + ";
      s += AZToNucleusLabel(At, Zt);
      s += " #rightarrow ";
      s += AZToNucleusLabel(Ar, Zr);
      return s;
    }

    int Ap;
    int Zp;
    int At;
    int Zt;
    int Ar;
    int Zr;

    ClassDef(ExcitReaction, 1);
};

class DDXSReaction : public Reaction {
  public:
    DDXSReaction() :
      Reaction(),
      ejectileA(0),
      ejectileZ(0)
  {}

    DDXSReaction(TFile *f, const int eA, const int eZ) :
      Reaction(f),
      ejectileA(eA),
      ejectileZ(eZ)
  {}

    DDXSReaction(std::vector<TFile *> const &f, const int eA, const int eZ) :
      Reaction(f.front()),
      ejectileA(eA),
      ejectileZ(eZ)
  {}

    DDXSReaction(const int ap, const int zp, const float ep, const int at, const int zt, const int eA, const int eZ) :
      Reaction(ap, zp, ep, at, zt),
      ejectileA(eA),
      ejectileZ(eZ)
  {}

    virtual operator TString() const {
      TString s = Reaction::operator TString();
      s += " #rightarrow ";
      s += AZToLabel(ejectileA, ejectileZ);
      return s;
    }

    virtual TString getKey(const bool logX) const {
      TString s = "DDXSReaction_";
      if(logX)
        s += "logx_";
      else
        s += "linx_";
      s += toFilenameStem();
      s += "_";
      s += AZToLabel(ejectileA, ejectileZ);
      return s;
    }

    int ejectileA;
    int ejectileZ;

    ClassDef(DDXSReaction, 1);
};

class IDatafile : public TObject {
  public:
    IDatafile() {}
    IDatafile(TString const &file, TString const &au) {
      filename = file;
      author = au;
    }
    virtual ~IDatafile() {
    }

    IDatafile &operator=(IDatafile const &other) {
      filename = other.filename;
      author = other.author;
      return *this;
    }

    IDatafile(IDatafile const &other) : TObject(other) {
      *this = other;
    }

    TString filename;
    TString author;

    virtual bool operator==(IDatafile const &other) {
      return (filename==other.filename && author==other.author);
    }
    virtual bool operator<(IDatafile const &other) {
      return filename.CompareTo(other.filename.Data());
    }

    ClassDef(IDatafile, 1);
};

class IDatafile1D : public IDatafile {
  public:
    IDatafile1D() :
      IDatafile(),
      cachedGraph(NULL),
      cachedGraphScale(0.)
  {}
    IDatafile1D(TString const &file, TString const &au) :
      IDatafile(file, au),
      cachedGraph(NULL),
      cachedGraphScale(0.)
  {}
    virtual ~IDatafile1D() {}
    virtual TGraphErrors *getGraph(const Float_t scale=1.) {
      if(!cachedGraph || std::abs(scale-cachedGraphScale)>1E-5) {
        cachedGraphScale = scale;
        cachedGraph = getFreshGraph(scale);
      }
      return cachedGraph;
    }
    virtual TGraphErrors *getFreshGraph(const Float_t scale) = 0;

  private:
    TGraphErrors *cachedGraph;
    Float_t cachedGraphScale;

    ClassDef(IDatafile1D, 1);
};

class IDatafile2D : public IDatafile {
  public:
    IDatafile2D() :
      IDatafile(),
      cachedGraph(NULL),
      cachedGraphScale(0.)
  {}
    IDatafile2D(TString const &file, TString const &au) :
      IDatafile(file, au),
      cachedGraph(NULL),
      cachedGraphScale(0.)
  {}
    virtual ~IDatafile2D() {}
    virtual TGraph2DErrors *getGraph(const Float_t scale=1.) {
      if(!cachedGraph || std::abs(scale-cachedGraphScale)>1E-5) {
        cachedGraphScale = scale;
        cachedGraph = getFreshGraph(scale);
      }
      return cachedGraph;
    }
    virtual TGraph2DErrors *getFreshGraph(const Float_t scale=1.) = 0;

  private:
    TGraph2DErrors *cachedGraph;
    Float_t cachedGraphScale;

    ClassDef(IDatafile2D, 1);
};

// DDXS Datafile
class DDXSDatafile : public IDatafile1D {
  public:
    DDXSDatafile() : IDatafile1D() {}
    DDXSDatafile(TString const &file, TString const &au, const double theta) :
      IDatafile1D(file, au),
      thetaMin(TMath::Max(0., theta-thetaAcceptance)),
      thetaMax(TMath::Min(180., theta+thetaAcceptance)),
      momentum(false)
  {
        fillPrivateMembers();
      }
    ~DDXSDatafile() {
    }

    TGraphErrors *getFreshGraph(const Float_t scale=1.) {
      std::vector<float> x, xerr, y, yerr;
      const char *cfilename = filename.Data();
      std::ifstream file(cfilename);
      Int_t nLines=0;
      Float_t xtemp, xerrtemp, ytemp, yerrtemp;
      while(file.good()) {
        std::string line;
        std::getline(file, line);
        std::stringstream lineStream(line);
        lineStream >> xtemp >> xerrtemp >> ytemp >> yerrtemp;
        if(lineStream.fail())
          continue;
        x.push_back(xtemp);
        xerr.push_back(xerrtemp);
        y.push_back(scale*ytemp);
        yerr.push_back(scale*yerrtemp);
        nLines++;
      }

      return new TGraphErrors(nLines, &x[0], &y[0], &xerr[0], &yerr[0]);
    }

    double getThetaMin() const { return thetaMin; }
    void setThetaMin(const double t) { thetaMin = t; }
    double getThetaMax() const { return thetaMax; }
    void setThetaMax(const double t) { thetaMax = t; }
    double isMomentum() const { return momentum; }
    void setMomentum(const double m) { momentum = m; }

    static const double thetaAcceptance;

  private:
    double thetaMin;
    double thetaMax;
    bool momentum;

    void fillPrivateMembers() {
      TPRegexp reThetaMin("^#\\s*thetamin\\s+([\\d.]+)");
      TPRegexp reThetaMax("^#\\s*thetamax\\s+([\\d.]+)");
      std::ifstream f(filename.Data());
      std::string buffer;
      while(std::getline(f, buffer)) {
        TString line(buffer);
        // parse thetaMin
        TObjArray *objs = reThetaMin.MatchS(line,"i");
        TObjString *subStr = (TObjString *)objs->At(0);
        if(subStr && !subStr->GetString().IsNull()) {
          thetaMin = ((TObjString *)objs->At(1))->GetString().Atof();
          std::cout << "thetaMin set to " << thetaMin << std::endl;
        }
        delete objs;
        // parse thetaMax
        objs = reThetaMax.MatchS(line,"i");
        subStr = (TObjString *)objs->At(0);
        if(subStr && !subStr->GetString().IsNull()) {
          thetaMax = ((TObjString *)objs->At(1))->GetString().Atof();
          std::cout << "thetaMax set to " << thetaMax << std::endl;
        }
        delete objs;
      }
    }

    ClassDef(DDXSDatafile, 1);
};

// charge-changing Datafile
class CCDatafile : public IDatafile1D {
  public:
    CCDatafile() : IDatafile1D() {}
    CCDatafile(TString const &file, TString const &au) :
      IDatafile1D(file, au) {}
    ~CCDatafile() {
    }

    TGraphErrors *getFreshGraph(const Float_t scale=1.) {
      std::vector<float> x, y, yerr;
      const char *cfilename = filename.Data();
      std::ifstream file(cfilename);
      Int_t nLines=0;
      Float_t xtemp, ytemp, yerrtemp;
      while(file.good()) {
        std::string line;
        std::getline(file, line);
        std::stringstream lineStream(line);
        lineStream >> xtemp >> ytemp >> yerrtemp;
        if(lineStream.fail())
          continue;
        x.push_back(xtemp);
        y.push_back(scale*ytemp);
        yerr.push_back(scale*yerrtemp);
        nLines++;
      }

      return new TGraphErrors(nLines, &x[0], &y[0], NULL, &yerr[0]);
    }
    ClassDef(CCDatafile, 1);
};

// mass-distribution Datafile
class MassDatafile : public IDatafile1D {
  public:
    MassDatafile() : IDatafile1D() {}
    MassDatafile(TString const &file, TString const &au) :
      IDatafile1D(file, au) {}
    ~MassDatafile() {
    }

    TGraphErrors *getFreshGraph(const Float_t scale=1.) {
      std::vector<float> x, y, yerr;
      const char *cfilename = filename.Data();
      std::ifstream file(cfilename);
      Int_t nLines=0;
      Float_t xtemp, ytemp, yerrtemp;
      while(file.good()) {
        std::string line;
        std::getline(file, line);
        std::stringstream lineStream(line);
        lineStream >> xtemp >> ytemp >> yerrtemp;
        if(lineStream.fail())
          continue;
        x.push_back(xtemp);
        y.push_back(scale*ytemp);
        yerr.push_back(scale*yerrtemp);
        nLines++;
      }

      return new TGraphErrors(nLines, &x[0], &y[0], NULL, &yerr[0]);
    }
    ClassDef(MassDatafile, 1);
};

// excitation-function Datafile
class ExcitDatafile : public IDatafile1D {
  public:
    ExcitDatafile() : IDatafile1D() {}
    ExcitDatafile(TString const &file, TString const &au) :
      IDatafile1D(file, au) {}
    ~ExcitDatafile() {
    }

    TGraphErrors *getFreshGraph(const Float_t scale=1.) {
      std::vector<float> x, y, xerr, yerr;
      const char *cfilename = filename.Data();
      std::ifstream file(cfilename);
      Int_t nLines=0;
      Float_t xtemp, ytemp, xerrtemp, yerrtemp;
      while(file.good()) {
        std::string line;
        std::getline(file, line);
        std::stringstream lineStream(line);
        lineStream >> xtemp >> ytemp >> xerrtemp >> yerrtemp;
        if(lineStream.fail())
          continue;
        x.push_back(xtemp);
        y.push_back(scale*ytemp);
        xerr.push_back(xerrtemp);
        yerr.push_back(scale*yerrtemp);
        nLines++;
      }

      return new TGraphErrors(nLines, &x[0], &y[0], &xerr[0], &yerr[0]);
    }
    ClassDef(ExcitDatafile, 1);
};

// isotopic cross-section Datafile
class IsotDatafile : public IDatafile2D {
  public:
    IsotDatafile() :
      IDatafile2D(),
      cachedHisto(NULL)
  {}
    IsotDatafile(TString const &file, TString const &au) :
      IDatafile2D(file, au),
      cachedHisto(NULL)
  {}
    ~IsotDatafile() {
    }

    TGraph2DErrors *getFreshGraph(const Float_t scale=1.) {
      std::vector<Double_t> x, y, z, zerr;
      const char *cfilename = filename.Data();
      std::ifstream file(cfilename);
      Int_t nLines=0;
      Float_t xtemp, ytemp, ztemp, zerrtemp;
      while(file.good()) {
        std::string line;
        std::getline(file, line);
        std::stringstream lineStream(line);
        lineStream >> xtemp >> ytemp >> ztemp >> zerrtemp;
        if(!lineStream) {
          std::cout << "Can't parse this line: " << line << std::endl;
          continue;
        }
        x.push_back(xtemp);
        y.push_back(scale*ytemp);
        z.push_back(scale*ztemp);
        zerr.push_back(scale*zerrtemp);
        nLines++;
      }

      return new TGraph2DErrors(nLines, &x[0], &y[0], &z[0], NULL, NULL, &zerr[0]);
    }

    TH2F *getHisto(const Float_t scale=1.) {
      if(!cachedHisto) {
        std::vector<Float_t> s, serr;
        std::vector<Int_t> a, z;
        const char *cfilename = filename.Data();
        std::ifstream file(cfilename);
        Int_t atemp, ztemp;
        Float_t stemp, serrtemp;
        Int_t ZMax=0;
        Int_t AMax=0;
        while(file.good()) {
          std::string line;
          std::getline(file, line);
          std::stringstream lineStream(line);
          lineStream >> ztemp >> atemp >> stemp >> serrtemp;
          if(lineStream.fail())
            continue;
          a.push_back(atemp);
          z.push_back(ztemp);
          s.push_back(scale*stemp);
          serr.push_back(scale*serrtemp);
          if(atemp>AMax)
            AMax=atemp;
          if(ztemp>ZMax)
            ZMax=ztemp;
        }

        TString histoName = "h_" + author;
        cachedHisto = new TH2F(histoName.Data(), histoName.Data(),
                               AMax+3, -0.5, AMax+2.5,
                               ZMax+3, -0.5, ZMax+2.5);
        const int n = a.size();
        for(int i=0; i<n; ++i) {
          const int A = a[i]+1;
          const int Z = z[i]+1;
          cachedHisto->SetBinContent(A, Z, s[i]);
          cachedHisto->SetBinError(A, Z, serr[i]);
        }
        cachedHisto->Sumw2();
      }
      return cachedHisto;
    }

  private:
    TH2F *cachedHisto;

    ClassDef(IsotDatafile, 1);
};

// angular-distribution Datafile
class AngDistDatafile : public IDatafile1D {
  public:
    AngDistDatafile() : IDatafile1D(), threshold(0.) {}
    AngDistDatafile(TString const &file, TString const &au) :
      IDatafile1D(file, au),
      threshold(0.)
  {
    fillThreshold();
  }
    ~AngDistDatafile() {
    }

    TGraphErrors *getFreshGraph(const Float_t scale=1.) {
      std::vector<float> x, y, xerr, yerr;
      const char *cfilename = filename.Data();
      std::ifstream file(cfilename);
      Int_t nLines=0;
      Float_t xtemp, ytemp, xerrtemp, yerrtemp;
      while(file.good()) {
        std::string line;
        std::getline(file, line);
        std::stringstream lineStream(line);
        lineStream >> xtemp >> xerrtemp >> ytemp >> yerrtemp;
        if(lineStream.fail())
          continue;
        x.push_back(xtemp);
        y.push_back(scale*ytemp);
        xerr.push_back(xerrtemp);
        yerr.push_back(scale*yerrtemp);
        nLines++;
      }

      return new TGraphErrors(nLines, &x[0], &y[0], &xerr[0], &yerr[0]);
    }

    double getThreshold() const { return threshold; }
    void setThreshold(const double t) { threshold = t; }

  private:
    double threshold;

    void fillThreshold() {
      TPRegexp regexp("^#\\s*threshold\\s+([\\d.]+)");
      std::ifstream f(filename.Data());
      std::string buffer;
      threshold = 0.;
      while(std::getline(f, buffer)) {
        TString line(buffer);
        TObjArray *objs = regexp.MatchS(line,"i");
        TObjString *subStr = (TObjString *)objs->At(0);
        if(subStr && !subStr->GetString().IsNull()) {
          threshold = ((TObjString *)objs->At(1))->GetString().Atof();
          std::cout << "threshold set to " << threshold << std::endl;
          delete objs;
          break;
        }
        delete objs;
      }
    }

    ClassDef(AngDistDatafile, 1);
};

typedef std::list<IDatafile1D*> IDatafile1DList;
typedef std::list<IDatafile1D*>::iterator IDatafile1DListIter;

typedef std::list<DDXSDatafile*> DDXSDatafileList;
typedef std::list<DDXSDatafile*>::iterator DDXSDatafileListIter;

class DDXSDataset {
  public:
    DDXSDataset() :
      nSkipAnglesBefore(0),
      nSkipAnglesAfter(0)
  {}

    typedef std::map<Int_t, DDXSDatafileList*>::iterator iterator;
    typedef std::map<Int_t, DDXSDatafileList*>::const_iterator const_iterator;
    typedef std::map<Int_t, DDXSDatafileList*>::reverse_iterator reverse_iterator;
    typedef std::map<Int_t, DDXSDatafileList*>::const_reverse_iterator const_reverse_iterator;

    DDXSDatafileList * &operator[](const Int_t &i) {
      return theMap[i];
    }
    size_t size() const {
      return theMap.size();
    }
    iterator begin() {
      return theMap.begin();
    }
    iterator end() {
      return theMap.end();
    }
    reverse_iterator rbegin() {
      return theMap.rbegin();
    }
    reverse_iterator rend() {
      return theMap.rend();
    }
    void clear() {
      theMap.clear();
      nSkipAnglesBefore = 0;
      nSkipAnglesAfter = 0;
    }
    void erase(iterator position) {
      theMap.erase(position);
    }
    size_t erase(const Int_t &i) {
      return theMap.erase(i);
    }
    iterator find(const Int_t &i) {
      return theMap.find(i);
    }
    bool empty() const {
      return theMap.empty();
    }

    Int_t getNSkipAnglesBefore() const { return nSkipAnglesBefore; }
    Int_t getNSkipAnglesAfter() const { return nSkipAnglesAfter; }

    void setNSkipAnglesBefore(const int n) { nSkipAnglesBefore = n; }
    void setNSkipAnglesAfter(const int n) { nSkipAnglesAfter = n; }

  private:
    std::map<Int_t, DDXSDatafileList*> theMap;
    Int_t nSkipAnglesBefore;
    Int_t nSkipAnglesAfter;
};

typedef DDXSDataset::iterator DDXSDatasetIter;

typedef std::map<TString, IDatafile1DList*> ExcitDataset;
typedef std::map<TString, IDatafile1DList*>::iterator ExcitDatasetIter;

extern TString expDataRoot;

extern Double_t energyTolerance;

enum Variable {
  MomentumOrEnergyVariable,
  pTVariable
};

DDXSDataset *findDDXSDataset(const DDXSReaction &reaction, const bool momentum=false, const Double_t minAngle=-1., const Double_t maxAngle=181.);
DDXSDataset *findDDXSpTDataset(const DDXSReaction &reaction, const Double_t minAngle=-1., const Double_t maxAngle=181.);
IDatafile1DList *findCCDatafileList(const Reaction &reaction);
IDatafile1DList *findMassDatafileList(const Reaction &reaction);
IDatafile1DList *findExcitDatafileList(const ExcitReaction &reaction);
ExcitDataset *findExcitReactionDataset(const Reaction &reaction, const int tATolerance=0, const int tZTolerance=0);
ExcitDataset *findExcitResidueDataset(const Reaction &reaction, const int tATolerance=0, const int tZTolerance=0);
IsotDatafile *findIsotDatafile(const Reaction &reaction);
AngDistDatafile *findAngDistDatafile(const DDXSReaction &reaction);
std::vector<TGraphErrors*> getGraphs(DDXSDataset * const theDataset, const double scaleFactor=0.1);
std::vector<TGraphErrors*> getGraphs(std::pair<Int_t,DDXSDatafileList*> const &theRecord, const Style_t markerStyle=24, const double scale=1.);
std::pair<double, double> getThetaMinMax(std::pair<Int_t,DDXSDatafileList*> const &theRecord);
std::pair<double, double> getAngleRange(std::pair<Int_t,DDXSDatafileList*> const &theRecord);
std::pair<double, double> getDatasetRange(DDXSDataset * const theDataset);
std::pair<double, double> getAngleYRange(std::pair<Int_t,DDXSDatafileList*> const &theRecord);
std::pair<double, double> getDatasetYRange(DDXSDataset * const theDataset);
void setExpDataRoot(const TString &r);
TString getExpDataRoot();
void setEnergyTolerance(const Double_t t);
Double_t getEnergyTolerance();
TString constructDDXSDatasetName(DDXSReaction const &reaction);
TString constructAngDistDatasetName(DDXSReaction const &reaction);
DDXSDataset *getEmptyDDXSDataset(const bool momentum=false, const Double_t minAngle=-1., const Double_t maxAngle=181., const Int_t nDatafiles=7);

#endif // DATASETLIBRARY_H_
