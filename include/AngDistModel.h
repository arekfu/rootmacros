#ifndef ANGDISTMODEL_H
#define ANGDISTMODEL_H
#include "TString.h"
#include "TFile.h"
#include "TH1F.h"
#include "DatasetLibrary.h"
#include "ModelLook.h"
#include <vector>
#include <utility>

class AngDistHisto {
  public:
    AngDistHisto() :
      histo(NULL)
  {}

    AngDistHisto(TH1F * const h, const char * const t) :
      histo(h),
      title(t)
  {}

    TH1F *getHisto() const {
      return histo;
    }

    ~AngDistHisto() {
    }

    TH1F *histo;
    TString title;

};

typedef std::vector<AngDistHisto> AngDistHistoVector;
typedef std::vector<AngDistHisto>::iterator AngDistHistoIter;

class AngDistModel {
  public:
    AngDistModel() :
      file(NULL)
  {}

    AngDistModel(const char *f, const char *t) :
      filename(f),
      title(t),
      file(NULL)
  {}

    ~AngDistModel() {
    }

    TFile *getFile() {
      if(!file)
        file = new TFile(filename);
      return file;
    }

    AngDistHisto *getHisto(TString const &system) {
      TFile *f = getFile();
      TString histoName = "h_angdist_" + system(18, system.Length()-18);
      TH1F *h;
      f->GetObject(histoName.Data(), h);
      if(h)
        return new AngDistHisto(h, title);
      else
        return NULL;
    }

    DDXSReaction *getReaction(TString const &system) {
      DDXSReaction *reaction;
      TFile *f = getFile();
      f->GetObject(system.Data(), reaction);
      return reaction;
    }

    TString filename;
    TString title;

  protected:
    TFile *file;

};

typedef std::vector<AngDistModel> AngDistModelVector;
typedef std::vector<AngDistModel>::iterator AngDistModelIter;

#ifdef __MAKECINT__
#pragma link C++ class AngDistModel;
#pragma link C++ class AngDistHisto;
#pragma link C++ class vector<AngDistModel>;
#pragma link C++ class vector<AngDistHisto>;
#pragma link C++ class vector<AngDistModel>::iterator-;
#pragma link C++ class vector<AngDistHisto>::iterator-;
#endif // __MAKECINT__

#endif
