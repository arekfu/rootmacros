#ifndef DDXSMODEL_H
#define DDXSMODEL_H
#include "TString.h"
#include "TFile.h"
#include "TH1F.h"
#include "DatasetLibrary.h"
#include "ModelLook.h"
#include <vector>
#include <utility>

class DDXSHistos {
  public:
    DDXSHistos() :
      histos(NULL)
  {}

    DDXSHistos(std::vector<TH1F*> * const h, const char * const t) :
      histos(h),
      title(t)
  {}

    TH1F *operator()(const int i) {
      return (*histos)[i];
    }

    size_t size() const {
      return histos->size();
    }

    TH1F *front() const {
      return histos->front();
    }

    TH1F *back() const {
      return histos->back();
    }

    ~DDXSHistos() {
    }

    std::vector<TH1F*> *histos;
    TString title;

};

typedef std::vector<DDXSHistos> DDXSHistosVector;
typedef std::vector<DDXSHistos>::iterator DDXSHistosIter;

class DDXSModel {
  public:
    DDXSModel() :
      file(NULL)
  {}

    DDXSModel(const char *f, const char *t) :
      filename(f),
      title(t),
      file(NULL)
  {}

    ~DDXSModel() {
    }

    TFile *getFile() {
      if(!file)
        file = new TFile(filename);
      return file;
    }

    DDXSHistos getHistos(TString const &system) {
      TFile *f = getFile();
      TString histoBaseName = "h_" + system(18, system.Length()-18);
      std::vector<TH1F *> *histos = new std::vector<TH1F *>;
      TH1F *h;
      for(int i=0; ; ++i) {
        TString histoName = histoBaseName + "_";
        histoName += i;
        f->GetObject(histoName.Data(), h);
        if(h)
          histos->push_back(h);
        else
          break;
      }
      return DDXSHistos(histos, title);
    }

    DDXSReaction *getReaction(TString const &system) {
      DDXSReaction *reaction;
      TFile *f = getFile();
      f->GetObject(system.Data(), reaction);
      return reaction;
    }

    TString filename;
    TString title;

  protected:
    TFile *file;

};

typedef std::vector<DDXSModel> DDXSModelVector;
typedef std::vector<DDXSModel>::iterator DDXSModelIter;

#ifdef __MAKECINT__
#pragma link C++ class DDXSModel;
#pragma link C++ class DDXSHistos;
#pragma link C++ class vector<DDXSModel>;
#pragma link C++ class vector<DDXSHistos>;
#pragma link C++ class vector<DDXSModel>::iterator-;
#pragma link C++ class vector<DDXSHistos>::iterator-;
#endif // __MAKECINT__

#endif
