#ifndef utils_h_
#define utils_h_

#include "TTree.h"
#include "TH1F.h"
#include "TString.h"
#include "TBranch.h"
#include "TLeaf.h"
#include <iostream>
#include <utility>

double maxEnergy(TTree *t) {
  return t->GetMaximum("EKin");
}

double maxEnergy(TTree *t, const Short_t Ae, const Short_t Ze) {
  int counts = 0;
  const int maxCounts = 10000;
  double maxE = 0.;
  for(Long64_t i=0; i<t->GetEntries() && counts<maxCounts; ++i) {
    t->GetEntry(i);
    // Must update these at every GetEntry because the TTree might be a TChain
    TLeaf *nParticlesLeaf = t->GetLeaf("nParticles");
    TLeaf *EKinLeaf = t->GetLeaf("EKin");
    TLeaf *ALeaf = t->GetLeaf("A");
    TLeaf *ZLeaf = t->GetLeaf("Z");
    Int_t nParticles = ((Int_t)nParticlesLeaf->GetValue());
    for(Int_t j=0; j<nParticles; ++j) {
      Float_t EKin = ((Float_t)EKinLeaf->GetValue(j));
      Short_t A = ((Short_t)ALeaf->GetValue(j));
      Short_t Z = ((Short_t)ZLeaf->GetValue(j));
      if(A==Ae && Z==Ze) {
        counts++;
        if(EKin > maxE)
          maxE = EKin;
      }
    }
  }
  return maxE;
}

void projectAZ(TTree *t, const char *name, const char *cut="") {
  std::cout << "Filling histogram " << name << " with cut " << cut << std::endl;
  t->Project(name, "Z:A", cut);
}

void projectEnergy(TTree *t, const char *name, const char *cut="") {
  std::cout << "Filling histogram " << name << " with cut " << cut << std::endl;
  t->Project(name, "EKin", cut);
}

void projectMomentum(TTree *t, const char *name, const char *cut="") {
  std::cout << "Filling momentum histogram " << name << " with cut " << cut << std::endl;
  t->Project(name, "sqrt(px**2+py**2+pz**2)", cut);
}

void projectTheta(TTree *t, const char *name, const char *cut="") {
  std::cout << "Filling histogram " << name << " with cut " << cut << std::endl;
  t->Project(name, "theta", cut);
}

void projectCharge(TTree *t, const char *name, const char *cut="") {
  std::cout << "Filling histogram " << name << " with cut " << cut << std::endl;
  t->Project(name, "Z", cut);
}

void projectMass(TTree *t, const char *name, const char *cut="") {
  std::cout << "Filling histogram " << name << " with cut " << cut << std::endl;
  t->Project(name, "A", cut);
}

void projectpT(TTree *t, const char *name, const char *cut="") {
  std::cout << "Filling histogram " << name << " with cut " << cut << std::endl;
  t->Project(name, "sqrt(px**2+py**2)", cut);
}


int findFirstBinBelow(TH1F * const h, const double errorThreshold) {
  int n = h->GetNbinsX();
  for(int i=h->GetMaximumBin(); i<n; ++i) {
    if(h->GetBinError(i)>errorThreshold*h->GetBinContent(i))
      return i;
  }
  return n;
}

int findLastBinAbove(TH1F * const h, const double errorThreshold) {
  for(int i=h->GetMaximumBin(); i>=0; --i) {
    if(h->GetBinError(i)>errorThreshold*h->GetBinContent(i))
      return i;
  }
  return 0;
}

double getCutThreshold();

void setCutThreshold(const double a);

void cutLowStatistics(TH1F * const h);

double getNormalisationFromGT(TTree * const gt);
int getApFromGT(TTree * const gt);
int getZpFromGT(TTree * const gt);
int getAtFromGT(TTree * const gt);
int getZtFromGT(TTree * const gt);

double plotNormalised(const char * const varexp, const char * const cut="", const char * const option="", TFile *file=NULL);

std::pair<int,int> getBestFitXY(const int n);

TH1F *averageHistos(TH1F *h1, TH1F *h2, const double w1=0.5, const double w2=0.5);
void averageAllHistos(const char *fname1, const char *fname2, const char *outfname, const double w1=0.5, const double w2=0.5);
#endif // utils_h_
