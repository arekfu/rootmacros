#include <TMath.h>

double rapidity(const double E, const double pz) {
  return TMath::ATanH(pz/E);
}
