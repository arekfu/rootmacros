#include "TString.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TStyle.h"

#include "DatasetLibrary.h"
#include <algorithm>
#include <iostream>
#include <vector>

Color_t getColor(const unsigned index) {
  static const Color_t colors[] = {
    kBlack,
    kRed,
    kBlue,
    kGreen+2,
    kOrange
  };

  if(index<sizeof(colors))
    return colors[index];
  else
    return ((Color_t)index);
}

Style_t getStyle(const unsigned index) {
  static const Style_t styles[] = {
    kSolid,
    kSolid,
    kSolid,
    kSolid,
    kSolid
  };

  if(index<sizeof(styles))
    return styles[index];
  else
    return ((Style_t)index);
}



/** \brief Plot a list of excitation curves
 *
 * \param files a colon-separated list of files
 * \param legends a colon-separated list of legends
 * \param plot title
 * \param outfile the name of the output file name
 */
void excitationFunctionPlot(TString files, TString legends, TString title, TString outfile, const bool logY=false) {

  TObjArray *filesArray = files.Tokenize(":");
  filesArray->SetOwner(kTRUE);
  const int nFiles = filesArray->GetEntries();

  TObjArray *legendsArray = legends.Tokenize(":");
  legendsArray->SetOwner(kTRUE);
  const int nLegends = legendsArray->GetEntries();

  for(int i=nLegends; i<nFiles; ++i) {
    legendsArray->AddLast(new TObjString(TString::Itoa(i,10)));
  }

  TCanvas *canvas = new TCanvas;

  // Minimum on the Y axis
  double xMin = 0.;
  double xMax = -1E250;
  double yMax = -1E250;
  double yMin;
  if(!logY)
    yMin = 0.;
  else
    yMin = 1E250;

  std::vector<TGraph *> theGraphs;

  // Loop over the files and get a Graph for each of them
  for(int i=0; i<nFiles; ++i) {
    TString theFile = ((TObjString *)filesArray->At(i))->GetString();
    CCDatafile *aDatafile = new CCDatafile(theFile, "");
    TGraph *theGraph = aDatafile->getGraph();
    delete aDatafile;

    // Update the min and max values
    xMax = std::max(xMax, theGraph->GetXaxis()->GetXmax());
    yMax = std::max(yMax, theGraph->GetYaxis()->GetXmax());
    if(logY)
      yMin = std::min(yMin, theGraph->GetYaxis()->GetXmin());

    // Set the colours, styles, etc.
    theGraph->SetLineColor(getColor(i));
    theGraph->SetLineStyle(getStyle(i));

    theGraphs.push_back(theGraph);
  }

  std::cerr << "xMin, yMin, xMax, yMax = "
    << xMin << '\t'
    << yMin << '\t'
    << xMax << '\t'
    << yMax << std::endl;

  if(logY)
    canvas->SetLogy();

  TH1F *theFrame = canvas->DrawFrame(xMin, yMin, xMax, yMax);
  theFrame->GetXaxis()->SetTitle("projectile energy [MeV]");
  theFrame->GetYaxis()->SetTitle("cross section [mb]");
  theFrame->SetTitle(title);
  canvas->Update();

  TLegend *theLegend = new TLegend(0.5*xMax,0.8*yMax,xMax,yMax,"","");
  theLegend->SetBorderSize(0);

  for(int i=0; i<nFiles; ++i) {
    TGraph *aGraph = theGraphs.at(i);
    aGraph->Draw("l x same");
    TString theLegendString = ((TObjString *)legendsArray->At(i))->GetString();
    theLegend->AddEntry(aGraph, theLegendString, "l");
  }

  theLegend->Draw();

  canvas->Update();

  // Save the plot
  canvas->Print(outfile);

  delete theLegend;

  for(int i=0; i<nFiles; ++i) {
    TGraph *aGraph = theGraphs.at(i);
    delete aGraph;
  }

  delete canvas;

  delete filesArray;
  delete legendsArray;
}

