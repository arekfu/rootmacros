#include <list>
#include <sstream>
#include <iostream>
#include "TTree.h"
#include "TH1.h"
#include "TH1F.h"
#include "TStyle.h"
#include "TFile.h"
#include "TROOT.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TPaveText.h"
#include "maxXFromHistos.h"

double maxEnergy(TTree *t, const bool isOld) {
	if(isOld)
		return t->GetMaximum("Enerj");
	else
		return t->GetMaximum("EKin");
}

void projectEnergy(TTree *t, const bool isOld, const char *name, const char *cut) {
	cout << "Filling histogram " << name << " with cut " << cut << endl;
	if(isOld)
		t->Project(name, "Enerj", cut);
	else
		t->Project(name, "EKin", cut);
}

void pionSpectraDDXS(char *fname1, char *fname2, float factor, char *fout) {
	const int nAngles = 6;
	const float theta[nAngles] = {0., 30., 60., 90., 120., 150.}; // degrees
	const float halfAcceptance = 10.; // degrees

	TH1F *hp1[nAngles], *hn1[nAngles], *hm1[nAngles];
	TH1F *hp2[nAngles], *hn2[nAngles], *hm2[nAngles];
	const int nBins = 60;

	gStyle->SetOptLogy();
	gStyle->SetOptDate(22);
	gStyle->GetAttDate()->SetTextSize(0.015);
	gStyle->SetOptStat(0);
	gStyle->SetFillColor(kWhite);
	gStyle->SetDrawBorder(0);
	gStyle->SetOptTitle(0);

	TFile f1(fname1);
	TTree *t1;
	bool isOld1 = true;
	t1 = (TTree *) gROOT->FindObject("h101");
	if(!t1) {
		t1 = (TTree *) gROOT->FindObject("et");
		isOld1 = false;
	}
	TFile f2(fname2);
	TTree *t2;
	bool isOld2 = true;
	t2 = (TTree *) gROOT->FindObject("h101");
	if(!t2) {
		t2 = (TTree *) gROOT->FindObject("et");
		isOld2 = false;
	}

	const double maxE1 = maxEnergy(t1, isOld1) - 100.;
	const double maxE2 = maxEnergy(t2, isOld2) - 100.;
	const double maxE  = TMath::Max(maxE1,maxE2);

	// Fill the histos
	for(int i=0; i<nAngles; ++i) {

		const double minTheta = TMath::Max(theta[i]-halfAcceptance,0.);
		const double maxTheta = TMath::Min(theta[i]+halfAcceptance,180.);
		const double solidAngle = cos(TMath::Pi()*minTheta/180.) - cos(TMath::Pi()*maxTheta/180.);
		const double factor1=pow(0.1,(double) i)/solidAngle;
		const double factor2=pow(0.1,(double) i)*factor/solidAngle;

		stringstream cutm1, cutn1, cutp1, cutm2, cutn2, cutp2;
		if(isOld1) {
			cutm1 << factor1 << "*(Avv==-1 && Zvv==-1 && Ityp==1 && Tetlab<" << maxTheta << " && Tetlab>" << minTheta << ")";
			cutn1 << factor1 << "*(Avv==-1 && Zvv==0 && Ityp==1 && Tetlab<" << maxTheta << " && Tetlab>" << minTheta << ")";
			cutp1 << factor1 << "*(Avv==-1 && Zvv==1 && Ityp==1 && Tetlab<" << maxTheta << " && Tetlab>" << minTheta << ")";
		} else {
			cutm1 << factor1 << "*(A==0 && Z==-1 && origin==-1 && theta<" << maxTheta << " && theta>" << minTheta << ")";
			cutn1 << factor1 << "*(A==0 && Z==0 && origin==-1 && theta<" << maxTheta << " && theta>" << minTheta << ")";
			cutp1 << factor1 << "*(A==0 && Z==1 && origin==-1 && theta<" << maxTheta << " && theta>" << minTheta << ")";
		}

		if(isOld2) {
			cutm2 << factor2 << "*(Avv==-1 && Zvv==-1 && Ityp==1 && Tetlab<" << maxTheta << " && Tetlab>" << minTheta << ")";
			cutn2 << factor2 << "*(Avv==-1 && Zvv==0 && Ityp==1 && Tetlab<" << maxTheta << " && Tetlab>" << minTheta << ")";
			cutp2 << factor2 << "*(Avv==-1 && Zvv==1 && Ityp==1 && Tetlab<" << maxTheta << " && Tetlab>" << minTheta << ")";
		} else {
			cutm2 << factor2 << "*(A==0 && Z==-1 && origin==-1 && theta<" << maxTheta << " && theta>" << minTheta << ")";
			cutn2 << factor2 << "*(A==0 && Z==0 && origin==-1 && theta<" << maxTheta << " && theta>" << minTheta << ")";
			cutp2 << factor2 << "*(A==0 && Z==1 && origin==-1 && theta<" << maxTheta << " && theta>" << minTheta << ")";
		}

		stringstream namep1, namep2, namen1, namen2, namem1, namem2;
		namep1 << "hp1_" << i;
		namep2 << "hp2_" << i;
		namen1 << "hn1_" << i;
		namen2 << "hn2_" << i;
		namem1 << "hm1_" << i;
		namem2 << "hm2_" << i;

		cout << "Creating histograms for i=" << i << endl;
		cout << "  names: " << namep1.str() << endl;
		cout << "         " << namep2.str() << endl;
		cout << "         " << namen1.str() << endl;
		cout << "         " << namen2.str() << endl;
		cout << "         " << namem1.str() << endl;
		cout << "         " << namem2.str() << endl;

		hp1[i] = new TH1F(namep1.str().c_str(),namep1.str().c_str(),nBins,0.,maxE);
		hp2[i] = new TH1F(namep2.str().c_str(),namep2.str().c_str(),nBins,0.,maxE);
		hp2[i]->SetLineColor(kRed);
		hn1[i] = new TH1F(namen1.str().c_str(),namen1.str().c_str(),nBins,0.,maxE);
		hn2[i] = new TH1F(namen2.str().c_str(),namen2.str().c_str(),nBins,0.,maxE);
		hn2[i]->SetLineColor(kRed);
		hm1[i] = new TH1F(namem1.str().c_str(),namem1.str().c_str(),nBins,0.,maxE);
		hm2[i] = new TH1F(namem2.str().c_str(),namem2.str().c_str(),nBins,0.,maxE);
		hm2[i]->SetLineColor(kRed);

		projectEnergy(t1, isOld1, namep1.str().c_str(), cutp1.str().c_str());
		projectEnergy(t2, isOld2, namep2.str().c_str(), cutp2.str().c_str());
		projectEnergy(t1, isOld1, namen1.str().c_str(), cutn1.str().c_str());
		projectEnergy(t2, isOld2, namen2.str().c_str(), cutn2.str().c_str());
		projectEnergy(t1, isOld1, namem1.str().c_str(), cutm1.str().c_str());
		projectEnergy(t2, isOld2, namem2.str().c_str(), cutm2.str().c_str());

	}

	hp1[0]->SetMinimum(0.001*hp1[nAngles-1]->GetMaximum());
	hn1[0]->SetMinimum(0.001*hn1[nAngles-1]->GetMaximum());
	hm1[0]->SetMinimum(0.001*hm1[nAngles-1]->GetMaximum());

	// Construct a list of histograms
	std::list<TH1*> histoList;
	for(int i=0; i<nAngles; ++i) {
		histoList.push_back(hp1[i]);
		histoList.push_back(hm1[i]);
		histoList.push_back(hn1[i]);
		histoList.push_back(hp2[i]);
		histoList.push_back(hm2[i]);
		histoList.push_back(hn2[i]);
	}

	// Determine maximum x value
	cout << "Determining maximum x value" << endl;
	const double maxEPlot=1.1*maxXFromHistos(histoList);
	cout << "Maximum x value should be " << maxEPlot << endl;

	TCanvas canvas("canvas","canvas");
    TPad c1("c1","c1",0,0.023,1,1);
    c1.Draw();
	c1.Divide(3);
	c1.cd(1);
	gPad->DrawFrame(0., hp1[0]->GetMinimum(), maxEPlot, 2.*hp1[0]->GetMaximum());
	for(int i=0; i<nAngles; ++i) {
		cout << "Frame 1, drawing i=" << i << endl;
		hp1[i]->Draw("same");
		hp2[i]->Draw("same");
	}
	c1.cd(2);
	gPad->DrawFrame(0., hn1[0]->GetMinimum(), maxEPlot, 2.*hn1[0]->GetMaximum());
	for(int i=0; i<nAngles; ++i) {
		cout << "Frame 2, drawing i=" << i << endl;
		hn1[i]->Draw("same");
		hn2[i]->Draw("same");
	}
	c1.cd(3);
	gPad->DrawFrame(0., hm1[0]->GetMinimum(), maxEPlot, 2.*hm1[0]->GetMaximum());
	for(int i=0; i<nAngles; ++i) {
		cout << "Frame 3, drawing i=" << i << endl;
		hm1[i]->Draw("same");
		hm2[i]->Draw("same");
	}

	c1.cd();
	TPaveText *filesPave = new TPaveText(0.02,0.92,0.2,0.98,"NDC");
	filesPave->SetBorderSize(1);
	filesPave->AddText(fname1);
	stringstream line2;
	line2 << fname2 << " * " << factor;
	filesPave->AddText(line2.str().c_str());
	filesPave->Draw();

	TPaveText *anglesPave = new TPaveText(0.52,0.92,0.92,0.98,"NDC");
	anglesPave->SetBorderSize(1);
	stringstream angleSS;
	angleSS << "#theta = (";
	for(int i=0; i<nAngles-1; ++i) {
		angleSS << theta[i] << "#circ, ";
	}
	angleSS << theta[nAngles-1] << "#circ) #pm " << halfAcceptance << "#circ";
	anglesPave->AddText(angleSS.str().c_str());
	anglesPave->Draw();

	c1.cd(1);
	TPaveText *piplusPave = new TPaveText(0.2,0.87,0.4,0.93,"NDC");
	piplusPave->SetBorderSize(0);
	piplusPave->AddText("#pi^{+}");
	piplusPave->Draw();

	c1.cd(2);
	TPaveText *pizeroPave = new TPaveText(0.2,0.87,0.4,0.93,"NDC");
	pizeroPave->SetBorderSize(0);
	pizeroPave->AddText("#pi^{0}");
	pizeroPave->Draw();

	c1.cd(3);
	TPaveText *piminusPave = new TPaveText(0.2,0.87,0.4,0.93,"NDC");
	piminusPave->SetBorderSize(0);
	piminusPave->AddText("#pi^{-}");
	piminusPave->Draw();

	canvas.Print(fout);
}
