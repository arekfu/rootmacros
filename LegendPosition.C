#include "TLegend.h"
#include "TBox.h"
#include "TPad.h"
#include "TH1.h"
#include "TGraph.h"
#include "TFrame.h"
#include "TMath.h"
#include <vector>
#include <iostream>

Double_t toXNDC(const double x) {
  gPad->Update();
  if(gPad->GetLogx())
    return (TMath::Log10(x) - gPad->GetX1())/(gPad->GetX2()-gPad->GetX1());
  else
    return (x - gPad->GetX1())/(gPad->GetX2()-gPad->GetX1());
}

Double_t toYNDC(const double y) {
  gPad->Update();
  if(gPad->GetLogy())
    return (TMath::Log10(y) - gPad->GetY1())/(gPad->GetY2()-gPad->GetY1());
  else
    return (y - gPad->GetY1())/(gPad->GetY2()-gPad->GetY1());
}

Int_t countPointsInside(TGraph *g, TBox *box) {
  int n=0;
  Double_t *gx = g->GetX();
  Double_t *gy = g->GetY();
  for(int i=0; i<g->GetN(); ++i) {
    double x, y;
    if(gPad->GetLogx())
      x = TMath::Log10(gx[i]);
    else
      x = gx[i];
    if(gPad->GetLogy())
      y = TMath::Log10(gy[i]);
    else
      y = gy[i];
    n += box->IsInside(x, y);
  }
  return n;
}

Int_t countPointsInside(TH1 *h, TBox *box) {
  int n=0;
  for(int i=1; i<h->GetNbinsX(); ++i) {
    double x, y;
    if(gPad->GetLogx())
      x = TMath::Log10(h->GetBinCenter(i));
    else
      x = h->GetBinCenter(i);
    if(gPad->GetLogy())
      y = TMath::Log10(h->GetBinContent(i));
    else
      y = h->GetBinContent(i);
    n += box->IsInside(x, y);
  }
  return n;
}

void bestLegendPosition(TLegend *theLegend, std::vector<TGraph*> *graphs, std::vector<TH1*> *histos, const Double_t legendWidth, const Double_t legendHeight) {
  TH1F *frame = (TH1F*)gPad->GetPrimitive("hframe");
  const double frameX1 = gPad->GetUxmin();
  const double frameX2 = gPad->GetUxmax();
  const double frameY1 = frame->GetYaxis()->GetXmin();
  const double frameY2 = frame->GetYaxis()->GetXmax();
  const double frameX1NDC = toXNDC(frameX1);
  const double frameX2NDC = toXNDC(frameX2);
  const double frameY1NDC = toYNDC(frameY1);
  const double frameY2NDC = toYNDC(frameY2);
  const double legendX1Min = frameX1NDC + 0.02*(frameX2NDC-frameX1NDC);
  const double legendX1Max = frameX2NDC-legendWidth - 0.02*(frameX2NDC-frameX1NDC);
  const double legendY1Min = frameY1NDC + 0.02*(frameY2NDC-frameY1NDC);
  const double legendY1Max = frameY2NDC-legendHeight - 0.02*(frameX2NDC-frameX1NDC);
  std::cout << "padX1=" << gPad->GetX1() << ", padX2=" << gPad->GetX2() << ", padY1=" << gPad->GetY1() << ", padY2=" << gPad->GetY2() << std::endl;
  std::cout << "frameX1=" << frameX1 << ", frameX2=" << frameX2 << ", frameY1=" << frameY1 << ", frameY2=" << frameY2 << std::endl;
  std::cout << "frameX1NDC=" << frameX1NDC << ", frameX2NDC=" << frameX2NDC << ", frameY1NDC=" << frameY1NDC << ", frameY2NDC=" << frameY2NDC << std::endl;
  std::cout << "legendX1Min=" << legendX1Min << ", legendX1Max=" << legendX1Max << ", legendY1Min=" << legendY1Min << ", legendY1Max=" << legendY1Max << std::endl;
  const int nSteps = 5;
  int minNCollisions = 1000000;
  int minix=0, miniy=0;
  theLegend->SetX1NDC(0.);
  theLegend->SetY1NDC(0.);
  theLegend->SetX2NDC(1.);
  theLegend->SetY2NDC(1.);
  theLegend->ConvertNDCtoPad();
  for(int ix=0; ix<nSteps; ++ix) {
    for(int iy=nSteps-1; iy>=0; --iy) {
      const double legendX1 = legendX1Min+ix*(legendX1Max-legendX1Min)/((double)(nSteps-1));
      const double legendY1 = legendY1Min+iy*(legendY1Max-legendY1Min)/((double)(nSteps-1));
      theLegend->SetX1NDC(legendX1);
      theLegend->SetY1NDC(legendY1);
      theLegend->SetX2NDC(legendX1+legendWidth);
      theLegend->SetY2NDC(legendY1+legendHeight);
      theLegend->ConvertNDCtoPad();
      int nCollisions=0;
      if(graphs) {
        for(std::vector<TGraph*>::iterator ig=graphs->begin(), eg=graphs->end(); ig!=eg; ++ig)
          nCollisions += countPointsInside(*ig, theLegend);
      }
      if(histos) {
        for(std::vector<TH1*>::iterator ih=histos->begin(), eh=histos->end(); ih!=eh; ++ih)
          nCollisions += countPointsInside(*ih, theLegend);
      }
      std::cout << "ix=" << ix << ", iy=" << iy << ", legendX1=" << theLegend->GetX1() << ", legendY1=" << theLegend->GetY1() << ", nCollisions=" << nCollisions << std::endl;
      if(ix==0 && iy==0)
        minNCollisions = nCollisions;
      else if(nCollisions<minNCollisions) {
        minNCollisions = nCollisions;
        minix = ix;
        miniy = iy;
      }
    }
  }

  const double legendX1 = legendX1Min+minix*(legendX1Max-legendX1Min)/((double)(nSteps-1));
  const double legendY1 = legendY1Min+miniy*(legendY1Max-legendY1Min)/((double)(nSteps-1));
  std::cout << "minix=" << minix << ", miniy=" << miniy << ", legendX1=" << legendX1 << ", legendY1=" << legendY1 << std::endl;
  theLegend->SetX1NDC(legendX1);
  theLegend->SetY1NDC(legendY1);
  theLegend->SetX2NDC(legendX1+legendWidth);
  theLegend->SetY2NDC(legendY1+legendHeight);
  theLegend->ConvertNDCtoPad();
}
