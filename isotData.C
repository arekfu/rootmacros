#include <list>
#include <sstream>
#include <iostream>
#include "TTree.h"
#include "TH2F.h"
#include "TH1D.h"
#include "TStyle.h"
#include "TFile.h"
#include "TROOT.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TPaveText.h"
#include "TKey.h"
#include "TLeaf.h"
#include "TLegend.h"
#include "ParticleType.h"
#include "DatasetLibrary.h"
#include "utils.h"
#include "IsotModel.h"
#include "LegendPosition.h"
#include <iostream>
#include <sstream>
#include <vector>

enum IsoX {
  Isotopic,
  Isobaric,
  Isotonic
};

int ZMinPlot = -1;
int ZMaxPlot = -1;
int getZMinPlot() { return ZMinPlot; }
int getZMaxPlot() { return ZMaxPlot; }
void setZMinPlot(const int z) { ZMinPlot = z; }
void setZMaxPlot(const int z) { ZMaxPlot = z; }

int AMinPlot = -1;
int AMaxPlot = -1;
int getAMinPlot() { return AMinPlot; }
int getAMaxPlot() { return AMaxPlot; }
void setAMinPlot(const int z) { AMinPlot = z; }
void setAMaxPlot(const int z) { AMaxPlot = z; }

int NMinPlot = -1;
int NMaxPlot = -1;
int getNMinPlot() { return NMinPlot; }
int getNMaxPlot() { return NMaxPlot; }
void setNMinPlot(const int z) { NMinPlot = z; }
void setNMaxPlot(const int z) { NMaxPlot = z; }

TString constructIsotDatafileName(Reaction const &reaction) {
  // Define the datafile name
  TString targetName = elementName(reaction.Zt);
  if(reaction.At!=0)
    targetName += reaction.At;
  TString energyString;
  energyString += reaction.Ep;
  TString datafileName = AZToSuffix(reaction.Ap, reaction.Zp)
    + "_"
    + targetName
    + "_"
    + energyString;
  return datafileName;
}

TH2F *isotHisto(TFile *f, const char *ID) {
  f->cd();
  TTree *et = (TTree *) gROOT->FindObject("et");
  TTree *gt = (TTree *) gROOT->FindObject("gt");

  // Read the run characteristics
  gt->GetEntry(gt->GetEntries()-1);
  Reaction reaction(f);

  // Compute the normalisation factors
  const Double_t normalisation =
    gt->GetLeaf("geometricCrossSection")->GetValue()/(gt->GetLeaf("nShots")->GetValue());

  // Fill the histos
  std::stringstream name;
  name << "hisot_" << ID;
  cout << "Creating histogram " << name.str() << endl;

  IsotDatafile *theDatafile = findIsotDatafile(reaction);
  TH2F *h;
  if(theDatafile) {
    TH2F *hData = theDatafile->getHisto();
    h = (TH2F *) hData->Clone(name.str().c_str());
  } else
    h = new TH2F(name.str().c_str(), name.str().c_str(), 250, -0.5, 249.5, 120, -0.5, 119.5);

  projectAZ(et, name.str().c_str());
  h->Sumw2();

  h->Scale(normalisation);

  return h;
}

int getNMin(TH2F const * const h) {
  int NMin=9999;
  const int nBinsX = h->GetNbinsX();
  const int nBinsY = h->GetNbinsY();
  for(int iA=1; iA<=nBinsX; ++iA) {
    const int A = (int) h->GetXaxis()->GetBinCenter(iA);
    if(A<=0)
      continue;
    for(int iZ=1; iZ<=nBinsY; ++iZ) {
      const int Z = (int) h->GetYaxis()->GetBinCenter(iZ);
      if(h->GetBinContent(iA,iZ)>0 && A-Z<NMin)
        NMin = A-Z;
    }
  }
  return NMin;
}

int getNMax(TH2F const * const h) {
  int NMax=-1;
  const int nBinsX = h->GetNbinsX();
  const int nBinsY = h->GetNbinsY();
  for(int iA=1; iA<=nBinsX; ++iA) {
    const int A = (int) h->GetXaxis()->GetBinCenter(iA);
    for(int iZ=1; iZ<=nBinsY; ++iZ) {
      const int Z = (int) h->GetYaxis()->GetBinCenter(iZ);
      if(h->GetBinContent(iA,iZ)>0 && A-Z>NMax)
        NMax = A-Z;
    }
  }
  return NMax;
}

TH1D *getMassDistribution(TH2F const * const h, const char * const ID, const TH2F * const hMask=NULL) {
  TString name = h->GetName();
  name += '_';
  name += ID;
  name += "_A";
  if(hMask) {
    TH2F *hTemp = (TH2F *) h->Clone("hTemp");
    std::cout << "hTemp: " << hTemp->GetNbinsX() << '\t' << hTemp->GetNbinsY() << std::endl;
    std::cout << "hMask: " << hMask->GetNbinsX() << '\t' << hMask->GetNbinsY() << std::endl;
    hTemp->Multiply(hMask);
    TH1D *massDistribution = hTemp->ProjectionX(name.Data(), 0, -1, "e");
    delete hTemp;
    return massDistribution;
  } else
    return h->ProjectionX(name.Data(), 0, -1, "e");
}

TH1D *getChargeDistribution(TH2F const * const h, const char * const ID, const TH2F * const hMask=NULL) {
  TString name = h->GetName();
  name += '_';
  name += ID;
  name += "_Z";
  if(hMask) {
    TH2F *hTemp = (TH2F *) h->Clone("hTemp");
    hTemp->Multiply(hMask);
    TH1D *massDistribution = hTemp->ProjectionY(name.Data(), 0, -1, "e");
    delete hTemp;
    return massDistribution;
  } else
    return h->ProjectionY(name.Data(), 0, -1, "e");
}

std::vector<TH1D*> *getIsotopicDistributions(TH2F const * const h, const char * const ID, int ZMin, int ZMax) {
  const TString baseName = h->GetName();
  std::vector<TH1D*> *histos = new std::vector<TH1D*>;
  for(int Z=ZMin; Z<=ZMax; ++Z) {
    TString name = baseName + '_';
    name += ID;
    name += "_isot_";
    name += Z;
    const int nBin = Z+1;
    TH1D *h1 = h->ProjectionX(name.Data(), nBin, nBin, "e");
    histos->push_back(h1);
  }
  return histos;
}

std::vector<TH1D*> *getIsobaricDistributions(TH2F const * const h, const char * const ID, int AMin, int AMax) {
  const TString baseName = h->GetName();
  std::vector<TH1D*> *histos = new std::vector<TH1D*>;
  for(int A=AMin; A<=AMax; ++A) {
    TString name = baseName + '_';
    name += ID;
    name += "_isob_";
    name += A;
    const int nBin = A+1;
    TH1D *h1 = h->ProjectionY(name.Data(), nBin, nBin, "e");
    histos->push_back(h1);
  }
  return histos;
}

std::vector<TH1D*> *getIsotonicDistributions(TH2F const * const h, const char * const ID, int NMin, int NMax) {
  const TString baseName = h->GetName();
  std::vector<TH1D*> *histos = new std::vector<TH1D*>;
  for(int N=NMin; N<=NMax; ++N) {
    TString name = baseName + '_';
    name += ID;
    name += "_isoton_";
    name += N;
    const double AMin = h->GetXaxis()->GetXmin();
    const double AMax = h->GetXaxis()->GetXmax();
    const int iAMin = (int) (AMin+0.5);
    const int iAMax = (int) (AMax+0.5);
    TH1D *hIsoton = new TH1D(name.Data(), name.Data(), AMax-AMin, AMin, AMax);
    for(int A=iAMin; A<=iAMax; ++A) {
      const int ABin = A+1;
      const int ZBin = (A-N)+1;
      hIsoton->SetBinContent(ABin, h->GetBinContent(ABin, ZBin));
      hIsoton->SetBinError(ABin, h->GetBinError(ABin, ZBin));
    }
    histos->push_back(hIsoton);
  }
  return histos;
}

std::vector<TH1D*> *isotHistos(TH2F const * const h, const char * const ID, TH2F const * const hMask=NULL, int ZMin=-1, int ZMax=-1, int AMin=-1, int AMax=-1, int NMin=-1, int NMax=-1) {
  if(!h)
    return NULL;

  TH1D *hA;
  TH1D *hZ;
  std::vector<TH1D *> *theHistos;

  if(hMask) {
    std::cout << "Using min and max values from another isotopic distributions: " << std::endl
      <<  "NMin=" << NMin << ", NMax=" << NMax << std::endl
      <<  "ZMin=" << ZMin << ", ZMax=" << ZMax << std::endl
      <<  "AMin=" << AMin << ", AMax=" << AMax << std::endl;
    // fill the isotopic distributions
    theHistos = getIsotopicDistributions(h, ID, ZMin, ZMax);
    std::vector<TH1D *> *theHistosIsobaric = getIsobaricDistributions(h, ID, AMin, AMax);
    theHistos->insert(theHistos->end(), theHistosIsobaric->begin(), theHistosIsobaric->end());
    delete theHistosIsobaric;
    std::vector<TH1D *> *theHistosIsotonic = getIsotonicDistributions(h, ID, NMin, NMax);
    theHistos->insert(theHistos->end(), theHistosIsotonic->begin(), theHistosIsotonic->end());
    delete theHistosIsotonic;

    // compute the mass and charge distributions
    hA = getMassDistribution(h, ID, hMask);
    hZ = getChargeDistribution(h, ID, hMask);
  } else {
    // compute the mass and charge distributions
    hA = getMassDistribution(h, ID);
    hZ = getChargeDistribution(h, ID);

    NMin = getNMin(h);
    NMax = getNMax(h);
    ZMin = hZ->FindFirstBinAbove()-1;
    ZMax = hZ->GetXaxis()->GetXmax();
    AMin = hA->FindFirstBinAbove()-1;
    AMax = hA->GetXaxis()->GetXmax();
    std::cout << "Min and max values determined from the isotopic distributions: " << std::endl
      <<  "NMin=" << NMin << ", NMax=" << NMax << std::endl
      <<  "ZMin=" << ZMin << ", ZMax=" << ZMax << std::endl
      <<  "AMin=" << AMin << ", AMax=" << AMax << std::endl;
    // fill the isotopic distributions
    theHistos = getIsotopicDistributions(h, ID, ZMin, ZMax);
    std::vector<TH1D *> *theHistosIsobaric = getIsobaricDistributions(h, ID, AMin, AMax);
    theHistos->insert(theHistos->end(), theHistosIsobaric->begin(), theHistosIsobaric->end());
    delete theHistosIsobaric;
    std::vector<TH1D *> *theHistosIsotonic = getIsotonicDistributions(h, ID, NMin, NMax);
    theHistos->insert(theHistos->end(), theHistosIsotonic->begin(), theHistosIsotonic->end());
    delete theHistosIsotonic;
  }

  theHistos->push_back(hA);
  theHistos->push_back(hZ);
  return theHistos;
}

TString getIsoXCanvasName(Reaction const &reaction, const int iPage, const IsoX isoX) {
  TString canvas = constructIsotDatafileName(reaction);
  if(isoX==Isotopic)
    canvas += "_cIsot";
  else if(isoX==Isobaric)
    canvas += "_cIsob";
  else if(isoX==Isotonic)
    canvas += "_cIsoton";
  canvas += TString::Format("%02d",iPage);
  return canvas;
}

TString getIsotMassOrChargeCanvasName(Reaction const &reaction, const bool isMass) {
  TString canvas = constructIsotDatafileName(reaction);
  if(isMass)
    canvas += + "_cA";
  else
    canvas += + "_cZ";
  return canvas;
}

std::vector<TCanvas*> *isotPlotIsoXCanvases(std::vector<TH1D*> *hIData, IsotHistosVector &histoss, const Reaction &reaction, const IsoX isoX, const int w0, const int wMin, const int wMax, char const * const suffix) {
  int wMinPlot, wMaxPlot;
  if(isoX==Isotopic) {
    if(ZMinPlot<0)
      wMinPlot = wMin;
    else
      wMinPlot = ZMinPlot;
    if(ZMaxPlot<0)
      wMaxPlot = wMax;
    else
      wMaxPlot = ZMaxPlot;
  } else if(isoX==Isobaric) {
    if(AMinPlot<0)
      wMinPlot = wMin;
    else
      wMinPlot = AMinPlot;
    if(AMaxPlot<0)
      wMaxPlot = wMax;
    else
      wMaxPlot = AMaxPlot;
  } else if(isoX==Isotonic) {
    if(NMinPlot<0)
      wMinPlot = wMin;
    else
      wMinPlot = NMinPlot;
    if(NMaxPlot<0)
      wMaxPlot = wMax;
    else
      wMaxPlot = NMaxPlot;
  }

  const int pages = (wMaxPlot-wMinPlot-1)/8+1;
  std::cout << "w values: " << w0 << '\t' << wMin << '\t' << wMax << '\t' << pages << '\n';
  std::cout << "          " << wMinPlot << '\t' << wMaxPlot << '\n';
  std::vector<TCanvas*> *theCanvases = new std::vector<TCanvas*>;

  // the plotting

  /*** isotopic distributions ***/
  for(int iPage=0; iPage<pages; ++iPage) {
    const int wMinPage = wMinPlot + iPage*8;

    // Get the max and min plot values for each row and column
    double hMax[8] = {1.E-20, 1.E-20, 1.E-20, 1.E-20, 1.E-20, 1.E-20, 1.E-20, 1.E-20};
    double hMin[8] = {1.E20, 1.E20, 1.E20, 1.E20, 1.E20, 1.E20, 1.E20, 1.E20};
    int AMin[8] = {9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999};
    int AMax[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    for(int j=0; j<8; ++j) {
      const int w = wMinPage + j;
      if(w<=wMaxPlot) {
        TH1D *hData = hIData->at(w0-wMin+wMinPlot+iPage*8+j);
        hMax[j] = TMath::Max(hData->GetMaximum(), hMax[j]);
        hMax[j] = TMath::Min(hData->GetMinimum(1E-20), hMin[j]);
        for(IsotHistosIter iH=histoss.begin(), e=histoss.end(); iH!=e; ++iH) {
          std::vector<TH1D*> *hh = iH->getHistos();
          TH1D *h = hh->at(w0-wMin+wMinPlot+iPage*8+j);
          hMax[j] = TMath::Max(h->GetMaximum(),hMax[j]);
          hMin[j] = TMath::Min(h->GetMinimum(1.E-20),hMin[j]);
        }
        int firstBin = hData->FindFirstBinAbove();
        if(firstBin>0)
          AMin[j] = firstBin-1;
        int lastBin = hData->FindLastBinAbove();
        if(lastBin>0)
          AMax[j] = lastBin-1;
      }
    }
    double hMaxRow[3];
    double hMinRow[3];
    int AMinCol[3];
    int AMaxCol[3];
    hMaxRow[0] = TMath::Max(hMax[0], hMax[1]);
    hMaxRow[1] = TMath::Max(TMath::Max(hMax[2], hMax[3]), hMax[4]);
    hMaxRow[2] = TMath::Max(TMath::Max(hMax[5], hMax[6]), hMax[7]);
    hMinRow[0] = TMath::Min(hMin[0], hMin[1]);
    hMinRow[1] = TMath::Min(TMath::Min(hMin[2], hMin[3]), hMin[4]);
    hMinRow[2] = TMath::Min(TMath::Min(hMin[5], hMin[6]), hMin[7]);
    AMaxCol[0] = TMath::Max(TMath::Max(AMax[0], AMax[2]), AMax[5])+2;
    AMaxCol[1] = TMath::Max(TMath::Max(AMax[1], AMax[3]), AMax[6])+2;
    AMaxCol[2] = TMath::Max(AMax[4], AMax[7])+2;
    AMinCol[0] = TMath::Min(TMath::Min(AMin[0], AMin[2]), AMin[5])+2;
    AMinCol[1] = TMath::Min(TMath::Min(AMin[1], AMin[3]), AMin[6])+2;
    AMinCol[2] = TMath::Min(AMin[4], AMin[7])+2;
    if(AMinCol[0]>9000 && AMinCol[1]>9000 && AMinCol[2]>9000) {
      std::cout << "breaking out of loop at iPage=" << iPage << ", wMinPage=" << wMinPage << '\n';
      break;
    }

    TString canvas = getIsoXCanvasName(reaction, iPage, isoX);
    TCanvas *c = new TCanvas(canvas.Data(), canvas.Data(), 297*4, 210*4);
    c->SetLeftMargin(0.25);
    c->SetTopMargin(0.02);
    c->SetRightMargin(0.02);
    c->SetBottomMargin(0.25);
    c->SetLogy();
    c->Divide(3, 3, 0., 0.);
    const double padSize = c->cd(1)->GetAbsHNDC();

    TString xAxisTitle, yAxisTitle;
    if(isoX==Isotopic) {
      xAxisTitle="mass number";
      yAxisTitle="#sigma(A) [mb]";
    } else if(isoX==Isobaric) {
      xAxisTitle="charge number";
      yAxisTitle="#sigma(Z) [mb]";
    } else if(isoX==Isotonic) {
      xAxisTitle="mass number";
      yAxisTitle="#sigma(A) [mb]";
    }
    for(int j=0; j<8; ++j) {
      const int subpad = ((j<2) ? (j+1) : (j+2));
      TVirtualPad *p = c->cd(subpad);
      p->SetLogy();
      p->SetGridx();
      const int w = wMinPage + j;
      const int iRow = (subpad-1)/3;
      const int iCol = (subpad-1)%3;
      const double xMin = AMinCol[iCol] - 0.5;
      const double xMax = AMaxCol[iCol] + 0.5;
      const double frameMax = 4.*hMaxRow[iRow];
      const double frameMin = 0.25*hMinRow[iRow];
      TH1F *frame = p->DrawFrame(xMin,frameMin,xMax,frameMax);
      frame->GetXaxis()->SetNdivisions(505);
      if(subpad==8) {
        frame->GetXaxis()->CenterTitle(true);
        frame->GetXaxis()->SetTitle(xAxisTitle);
        frame->GetXaxis()->SetTitleSize(0.2*padSize/p->GetAbsHNDC());
        frame->GetXaxis()->SetTitleOffset(0.8);
      }
      if(iRow==2) {
        frame->GetXaxis()->SetLabelOffset(0.015);
        frame->GetXaxis()->SetLabelSize(0.2*padSize/p->GetAbsHNDC());
      } else {
        frame->GetXaxis()->SetLabelOffset(100.);
        frame->GetXaxis()->SetLabelSize(0.);
      }
      frame->GetXaxis()->SetTickLength(-0.015);
      frame->GetXaxis()->SetLabelFont(42);
      frame->GetXaxis()->SetTitleFont(42);
      if(subpad==4) {
        frame->GetYaxis()->CenterTitle(true);
        frame->GetYaxis()->SetTitle(yAxisTitle);
        frame->GetYaxis()->SetTitleSize(0.2*padSize/p->GetAbsHNDC());
        frame->GetYaxis()->SetTitleOffset(0.8);
      }
      if(iCol==0) {
        frame->GetYaxis()->SetLabelOffset(0.015);
        frame->GetYaxis()->SetLabelSize(0.2*padSize/p->GetAbsHNDC());
      } else {
        frame->GetYaxis()->SetLabelOffset(100.);
        frame->GetYaxis()->SetLabelSize(0.);
      }
      frame->GetYaxis()->SetTickLength(-0.015);
      frame->GetYaxis()->SetLabelFont(42);
      frame->GetYaxis()->SetTitleFont(42);
      if(w>wMax)
        continue;
      resetModelLook();
      for(IsotHistosIter iH=histoss.begin(), e=histoss.end(); iH!=e; ++iH) {
        std::vector<TH1D*> *hh = iH->getHistos();
        TH1D *h = hh->at(w0-wMin+wMinPlot+iPage*8+j);
        ModelLook modelLook = getNextModelLook();
        h->SetLineColor(modelLook.color);
        h->SetLineStyle(modelLook.style);
        h->SetLineWidth(modelLook.width);
        h->Draw("same hist l ][");
      }
      Style_t markerStyle = 20;
      TH1D *hData = hIData->at(w0-wMin+wMinPlot+iPage*8+j);
      hData->SetMarkerSize(1.0);
      hData->SetMarkerStyle(markerStyle);
      hData->Draw("p e x0 same");

      const double xMinNDC = (xMin-p->GetX1())/(p->GetX2()-p->GetX1());
      const double frameMinNDC = (TMath::Log10(frameMin)-p->GetY1())/(p->GetY2()-p->GetY1());
      const double frameMaxNDC = (TMath::Log10(frameMax)-p->GetY1())/(p->GetY2()-p->GetY1());
      const double paveSize = 0.2;
      TPaveText *elementPave = new TPaveText(xMinNDC,frameMaxNDC-paveSize*(frameMaxNDC-frameMinNDC),xMinNDC+paveSize*(1.-xMinNDC),frameMaxNDC,"NDC");
      TString eName;
      if(isoX==Isotopic) {
        eName = "_{";
        eName += w;
        eName += "}";
        eName += elementName(w);
      } else if(isoX==Isobaric) {
        eName = "A=";
        eName += w;
      } else if(isoX==Isotonic) {
        eName = "N=";
        eName += w;
      }
      elementPave->SetBorderSize(1);
      TText *text = elementPave->AddText(eName);
      text->SetTextSize(text->GetTextSize()*padSize/p->GetAbsHNDC());
      elementPave->SetFillStyle(1001);
      elementPave->SetFillColor(kWhite);
      elementPave->Draw();
    }

    c->cd(3);
    TLegend *legend = new TLegend(0., 0., 1., 1., NULL, "brNDC");
    legend->SetBorderSize(0);
    for(IsotHistosIter iH=histoss.begin(), e=histoss.end(); iH!=e; ++iH) {
      std::vector<TH1D*> *hh = iH->getHistos();
      TH1D *h = hh->at(w0-wMin+wMinPlot+iPage*8);
      legend->AddEntry(h, iH->title);
    }
    legend->Draw();

    c->cd();

/*    TPaveText *legendPave = new TPaveText(0.5,0.96,0.9,0.99,"NDC");
    legendPave->SetBorderSize(0);
    legendPave->SetMargin(0);
    text = legendPave->AddText(label1);
    text->SetTextColor(color[0]);
    text->SetTextAlign(32);
    if(hasF2) {
      text = legendPave->AddText(label2);
      text->SetTextColor(color[1]);
      text->SetTextAlign(32);
    }
    legendPave->Draw();*/

    TString pdfFilename = canvas;
    TString canvasFilename = canvas;
    if(suffix) {
      pdfFilename += suffix;
      canvasFilename += suffix;
    }
    pdfFilename += ".pdf";
    canvasFilename += ".C";
    c->Print(pdfFilename.Data());
    c->SaveAs(canvasFilename.Data());
    theCanvases->push_back(c);
  }

  return theCanvases;
}

TCanvas *isotPlotMassOrChargeCanvas(TH1D *hData, IsotHistoVector &histos, const Reaction &reaction, const bool isMass, char const * const suffix) {

  // the plotting

  /*** mass or charge distribution ***/
  TString canvas = getIsotMassOrChargeCanvasName(reaction, isMass);
  TCanvas *c = new TCanvas(canvas.Data(), canvas.Data(), 297*4, 210*4);
  c->SetLeftMargin(0.11);
  c->SetTopMargin(0.02);
  c->SetRightMargin(0.02);
  c->SetBottomMargin(0.12);
  c->SetLogy();
  int iMin = hData->FindFirstBinAbove()-3;
  if(iMin<0)
    iMin=0;
  const double xMin = hData->GetBinCenter(iMin);
  const double xMax = hData->GetXaxis()->GetXmax();
  const double frameMin = 0.001*hData->GetMaximum();
  const double frameMax = 2.*hData->GetMaximum();
  TH1F *frame = c->DrawFrame(xMin,frameMin,xMax,frameMax);
  if(isMass)
    frame->GetXaxis()->SetTitle("mass number");
  else
    frame->GetXaxis()->SetTitle("charge number");
  frame->GetXaxis()->SetTickLength(-0.015);
  frame->GetXaxis()->SetTitleOffset(1.1);
  frame->GetXaxis()->SetLabelOffset(0.015);
  frame->GetYaxis()->SetTitleOffset(1.2);
  frame->GetYaxis()->SetLabelOffset(0.015);
  if(isMass)
    frame->GetYaxis()->SetTitle("#sigma(A) [mb]");
  else
    frame->GetYaxis()->SetTitle("#sigma(Z) [mb]");
  frame->GetYaxis()->SetTickLength(-0.015);

  resetModelLook();
  for(IsotHistoIter iH=histos.begin(), e=histos.end(); iH!=e; ++iH) {
    ModelLook modelLook = getNextModelLook();
    TH1D *h = iH->getHisto();
    h->SetLineColor(modelLook.color);
    h->SetLineStyle(modelLook.style);
    h->SetLineWidth(modelLook.width);
    h->Draw("same hist ][");
  }
  Style_t markerStyle = 20;
  hData->SetMarkerSize(1.0);
  hData->SetMarkerStyle(markerStyle);
  hData->Draw("p e x0 same");
  // determine the plot coordinates in NDC units
  const double xMinNDC = (xMin-c->GetX1())/(c->GetX2()-c->GetX1());
//  const double xMaxNDC = (xMax-c->GetX1())/(c->GetX2()-c->GetX1());
  const double frameMaxNDC = (TMath::Log10(frameMax)-c->GetY1())/(c->GetY2()-c->GetY1());
  TLegend *legend = new TLegend(0.5,0.2,0.9,0.45,TString(reaction),"brNDC");
  legend->SetBorderSize(0);
  std::vector<TH1*> *histosForLegend = new std::vector<TH1*>;
  histosForLegend->push_back(hData);
  for(IsotHistoIter iH=histos.begin(), e=histos.end(); iH!=e; ++iH) {
    legend->AddEntry(iH->getHisto(),iH->title);
    histosForLegend->push_back(iH->getHisto());
  }
  bestLegendPosition(legend, NULL, histosForLegend);
  legend->Draw();

  TString pdfFilename = canvas;
  TString canvasFilename = canvas;
  if(suffix) {
    pdfFilename += suffix;
    canvasFilename += suffix;
  }
  pdfFilename += ".pdf";
  canvasFilename += ".C";
  c->Print(pdfFilename.Data());
  c->SaveAs(canvasFilename.Data());

  return c;
}

std::vector<TCanvas*> *isotPlotCanvas(std::vector<TH1D*> *hIData, IsotHistosVector &histoss, const Reaction &reaction, const int ZMin, const int ZMax, const int AMin, const int AMax, const int NMin, const int NMax, char const * const suffix, const bool plotIsoX) {
  const int chargeIndex = hIData->size()-1;
  const int massIndex = hIData->size()-2;
  IsotHistoVector massHistos, chargeHistos;
  for(IsotHistosIter h=histoss.begin(), e=histoss.end(); h!=e; ++h) {
    massHistos.push_back(IsotHisto(h->getHistos()->at(massIndex), h->title));
    chargeHistos.push_back(IsotHisto(h->getHistos()->at(chargeIndex), h->title));
  }
  TCanvas *massCanvas = isotPlotMassOrChargeCanvas(hIData->at(massIndex),
                                           massHistos,
                                           reaction, true, suffix);
  TCanvas *chargeCanvas = isotPlotMassOrChargeCanvas(hIData->at(chargeIndex),
                                               chargeHistos,
                                               reaction, false, suffix);

  std::vector<TCanvas*> *theCanvases = isotPlotIsoXCanvases(hIData, histoss, reaction, Isotopic, 0, ZMin, ZMax, suffix);
  if(plotIsoX) {
    std::vector<TCanvas*> *theIsobaricCanvases = isotPlotIsoXCanvases(hIData, histoss, reaction, Isobaric, ZMax-ZMin+1, AMin, AMax, suffix);
    theCanvases->insert(theCanvases->end(), theIsobaricCanvases->begin(), theIsobaricCanvases->end());
    delete theIsobaricCanvases;
    std::vector<TCanvas*> *theIsotonicCanvases = isotPlotIsoXCanvases(hIData, histoss, reaction, Isotonic, ZMax-ZMin+AMax-AMin+2, NMin, NMax, suffix);
    theCanvases->insert(theCanvases->end(), theIsotonicCanvases->begin(), theIsotonicCanvases->end());
    delete theIsotonicCanvases;
  }
  theCanvases->push_back(massCanvas);
  theCanvases->push_back(chargeCanvas);
  return theCanvases;
}

std::vector<TCanvas*> *isotCanvas(IsotHisto2DVector &histos, const Reaction &reaction, char const * const suffix, const bool plotIsoX) {
  // Fetch the datafile
  IsotDatafile *theDatafile = findIsotDatafile(reaction);
  TString datafileName = constructIsotDatafileName(reaction);

  TH2F *hData = theDatafile->getHisto();
  std::vector<TH1D*> *hIsotData = isotHistos(hData, "data");

  // construct the mask for the measured isotopes
  TString maskName = hData->GetName();
  maskName += "_mask";
  TH2F *hMask = (TH2F*) hData->Clone(maskName.Data());
  hMask->Divide(hMask);

  // determine the lowest experimental charge distribution
  TH1D *hZ = hIsotData->at(hIsotData->size()-1);
  const int ZMin = hZ->FindFirstBinAbove() - 1;
  const int ZMax = hZ->GetXaxis()->GetXmax();
  TH1D *hA = hIsotData->at(hIsotData->size()-2);
  const int AMin = hA->FindFirstBinAbove() - 1;
  const int AMax = hA->GetXaxis()->GetXmax();
  const int NMin = getNMin(hData);
  const int NMax = getNMax(hData);

  IsotHistosVector histoss;

  int index=1;
  for(IsotHisto2DIter h=histos.begin(), e=histos.end(); h!=e; ++h, ++index) {
    TString indexString;
    indexString += index;
    std::vector<TH1D*> *hIsot = isotHistos(h->getHisto2D(), indexString, hMask, ZMin, ZMax, AMin, AMax, NMin, NMax);
    histoss.push_back(IsotHistos(hIsot, h->title));
  }

  return isotPlotCanvas(hIsotData, histoss, reaction, ZMin, ZMax, AMin, AMax, NMin, NMax, suffix, plotIsoX);
}

std::vector<TCanvas*> *isotCanvasFromTrees(char * const fname1, char * const fname2, char const * const label1, char const * const label2, char const * const suffix=NULL, const bool plotIsoX=false) {
  TFile *f1 = new TFile(fname1);
  // Fetch the dataset
  Reaction reaction(f1);
  TH2F *h1 = isotHisto(f1, "1");

  // the other file
  TFile *f2 = new TFile(fname2);
  TH2F *h2 = isotHisto(f2, "2");

  IsotHisto2DVector histos;
  histos.push_back(IsotHisto2D(h1, label1));
  histos.push_back(IsotHisto2D(h2, label2));

  // Call the function that does the real plotting
  return isotCanvas(histos, reaction, suffix, plotIsoX);
}

std::vector<TCanvas*> *isotCanvasFromTrees(char * const fname1, char * const fname2, char * const fname3, char const * const label1, char const * const label2, char const * const label3, char const * const suffix=NULL, const bool plotIsoX=false) {
  TFile *f1 = new TFile(fname1);
  // Fetch the dataset
  Reaction reaction(f1);
  TH2F *h1 = isotHisto(f1, "1");

  // the other file
  TFile *f2 = new TFile(fname2);
  TH2F *h2 = isotHisto(f2, "2");

  // the other other file
  TFile *f3 = new TFile(fname3);
  TH2F *h3 = isotHisto(f3, "3");

  IsotHisto2DVector histos;
  histos.push_back(IsotHisto2D(h1, label1));
  histos.push_back(IsotHisto2D(h2, label2));
  histos.push_back(IsotHisto2D(h3, label3));

  // Call the function that does the real plotting
  return isotCanvas(histos, reaction, suffix, plotIsoX);
}

void isotCanvasFromHistos(IsotModelVector &models, char const * const suffix=NULL, const bool plotIsoX=false) {
  IsotModel &firstModel = models.front();
  TFile *f1 = firstModel.getFile();

  // Get the list of datasets from the first file
  TList *keys = f1->GetListOfKeys();
  TIter next(keys);
  TKey *k;
  std::vector<TString> systems;
  while((k = ((TKey *)next()))) {
    TString name = k->GetName();
    if(name.BeginsWith("reaction"))
      systems.push_back(name);
  };
  std::cout << "Will process " << systems.size() << " systems." << std::endl;

  // Loop over all the systems
  for(std::vector<TString>::iterator iter=systems.begin(); iter!=systems.end(); ++iter) {
    Reaction *reaction = firstModel.getReaction(*iter);

    IsotHisto2DVector histos;
    for(IsotModelIter iModel=models.begin(), eModel=models.end(); iModel!=eModel; ++iModel) {
      IsotHisto2D *h = iModel->getHisto(*iter);
      if(h)
        histos.push_back(*h);
    }

    if(histos.size()!=models.size()) // missing models
      continue;

    // Call the function that does the real plotting
    std::vector<TCanvas *> *theCanvases = isotCanvas(histos, *reaction, suffix, plotIsoX);
    for(unsigned int i=0; i<theCanvases->size(); ++i)
      delete (*theCanvases)[i];
    delete theCanvases;
  }
}

void isotCanvasFromHistos(char const * const fname1, char const * const fname2, char const * const label1, char const * const label2, char const * const suffix=NULL, const bool plotIsoX=false) {
  IsotModelVector models;
  models.push_back(IsotModel(fname1, label1));
  models.push_back(IsotModel(fname2, label2));
  isotCanvasFromHistos(models, suffix, plotIsoX);
}
void saveIsot(char const * const inFilename, char const * const outFilename) {
  TFile *inFile = new TFile(inFilename);
  TFile *outFile = new TFile(outFilename, "UPDATE", outFilename, 9); // maximum compression

  Reaction reaction(inFile);
  TString datafileName = constructIsotDatafileName(reaction);

  TH2F *h = isotHisto(inFile, datafileName.Data());

  outFile->cd();
  h->Write();

  // Add the reaction to the output file
  TString reactionObjectName = "reaction_";
  reactionObjectName += datafileName;
  outFile->WriteObject(&reaction, reactionObjectName.Data());

  outFile->Write();
  outFile->Close();
  delete outFile;
  inFile->Close();
  delete inFile;
}

void isotASCIITFileToFile(TFile *f, char const * const ofname) {
  // Fetch the dataset
  TH2F *h = isotHisto(f, "1");

  std::ofstream of(ofname, std::ios_base::trunc);

  for(int i=1; i<=h->GetNbinsX(); ++i) {
    for(int j=1; j<=h->GetNbinsY(); ++j) {
      const double xsec =  h->GetBinContent(i,j);
      if(xsec>0) {
        of << (int)h->GetXaxis()->GetBinCenter(i)
          << '\t' <<  (int)h->GetYaxis()->GetBinCenter(j)
          << '\t' <<  xsec
          << '\t' <<  h->GetBinError(i,j)
          << std::endl;
      }
    }
  }

  of.close();

}

void isotASCIIFromTree(char * const fname, char * const ofsuffix=NULL) {
  TFile *f = new TFile(fname);
  // Fetch the dataset
  Reaction reaction(f);

  TString outputFilename = reaction.toFilenameStem();
  outputFilename += "_isot";
  if(ofsuffix)
    outputFilename += ofsuffix;
  outputFilename += ".dat";

  isotASCIITFileToFile(f, outputFilename.Data());
}

void isotASCIIFromTreeToFile(char * const fname, char const * const ofname) {
  TFile *f = new TFile(fname);

  isotASCIITFileToFile(f, ofname);
}

#ifdef __MAKECINT__
#pragma link C++ class vector<TH1D*>;
#endif // __MAKECINT__
